# 4. Test framework

Date: 2022-01-30

## Status

Accepted

## Context

I want to safely add features in a dynamically typed language.

## Solutions

1. Jest - very popular test framework with more tools/helper for mocking, reporting and coverage built-in.
   I think I don't need mocking library because in JS I can create manual test mocks / fixtures / doubles in simple way.
   Jest has very slow startup time. I think I don't need all of this extended functionality in this case.
  
2. Mocha - very popular test runner and test reporter.
   Very fast startup time, relatively small and just enough helpers included. It has very good integration with my IDE (WebStorm).


## Decision

I choose mocha as a reasonable balance between speed and usability
