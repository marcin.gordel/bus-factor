# 2. Language

Date: 2022-01-30

## Status

Accepted

## Context

I need simple code for fast developing PoC app. Complexity of application is small.
App run as script and simple imperative algorithm. For this functionality I can use simple, pure JS or TypeScript and Node.js.
Small js libraries is sufficient. I don't need any frameworks for this purpose.

## Solutions

1. JavaScript
2. TypeScript

## Decision

I chose pure JS natively supported by Node.js. I did not choose TypeScript
because simple JS is faster to develop such applications (with small complexity and poor domain).
TypeScript provides more extended syntax structures such as static typing and interfaces
but on the other hands it requires more configuration (for compile and build) and more code structures.
I think for this kind of application with one module, one bounded context, and a poor domain, JS is more appropriate.

## Consequences

If the domain grows in the future we need to take care of code complexity without static typing and explicit interfaces.
The data flow check will have to be tested by many automated test suites.
