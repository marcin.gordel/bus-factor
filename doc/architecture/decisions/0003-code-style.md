# 3. Code Style

Date: 2022-01-30

## Status

Accepted

## Context

I want to have consistent coding style, easy to read easy to extend and easy to automated test.

## Solutions

Class based style (full of this binding traps)
```javascript
class A {
    constructor() {
        this.state = {};
    }

    static factory() {
        return new A();
    }

    method() {
        return this.state;
    }
};
```

Thisless style (less memory optimized, but more predictable)
```javascript
const A = () => {
    const state = {};

    return {
        method() {
            return state;
        }
    }
};

A.factory = () => {
    return A();
};
```

## Decision
I found very detailed mental JS model explanation.
https://github.com/getify/You-Dont-Know-JS/blob/1st-ed/this%20%26%20object%20prototypes/ch6.md#mental-models-compared
https://www.youtube.com/watch?v=n9qzwI4Krmo

I decide to use more predictable thisless style.

## Consequences

Thisless styles relies only on 2 language features: functions/closures and object literals.
Functions closing over private state behave in the same way as this binding, but it is more predictable.
So we can get the same results, state and behavior travelling together in time with very basic language features.
