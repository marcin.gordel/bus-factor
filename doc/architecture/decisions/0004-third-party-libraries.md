# 4. Third-party libraries

Date: 2022-01-30

## Status

Accepted

## Context

1. We need to create CLI app for getting inputs args in correct and save way.
   
2. We need to use some tool for do http request to GitHub RestApi.
   
## Solutions

1. Own code
   - Solution A - Create own command line parser.
   - Solution B - Use Yargs as third-party library

2. Third party library
   - Solution A - Use standard node.js http module
   - Solution B - Use recommended by Github - Octokit library

## Decision

I choose Solution 2 - Third party libraries for CLI parser and http.

1. Yargs library has good reputations (>9k stars) ond GitHub. It is constantly developed and maintained and solved common issues on this domain.
   I think that reinventing the wheel is unnecessary in this case.
   
2. Octokit library - it is recommended library on GitHub official Rest APi Docs.
   Under the hood it used node-fetch library and provide simple configuration for github auth and headers. It is very fast to use.

## Consequences

I need to constantly update these dependencies to have a safe and non-vulnerable application.
If any of these libraries drop out of development, I will have to check for vulnerabilities myself and replace any weak dependencies.
