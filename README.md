# Bus Factor

[Bus factor](https://en.wikipedia.org/wiki/Bus_factor) is a measurement which attempts to estimate the number of key persons a project would need to lose in order for it to become stalled due to lack of expertise. It is commonly used in the context of software development.

For example, if a given project is developed by a single person, then the project's bus factor is equal to 1 (it's likely for the project to become unmaintained if the main contributor suddenly stops working on it).

This app allows you to find popular GitHub projects with a bus factor of 1.

Given a programming language name (`language`) and a project count (`project_count`), thi app fetch the first project_count most popular projects (sorted by the number of GitHub stars) from the given language.

For each project, app inspects its contributor statistics.
Project's bus factor is 1 if its most active developer's contributions account for **75% or more** of the total contributions count from the top 25 most active developers.

Finally, the projects with a bus factor of 75% are returned as output.

# Installation

```shell
npm install
```

Minimum Node.js version required: `14.0.0`

# Configuration 
Create .env file and define:
```shell
GITHUB_TOKEN=your_github_token
```

Additionally, You can also define in `src/Config.js` minimum threshold for Bus Factor (default 0.75) and sorting order for fetching GitHub project (default by stars).

# Run
```shell
node bus-factor.js
```

For help and description command line options:
```shell
node bus-factor.js --help
```

# Usage
```shell

Options:
  -l, --language       Programming language                            [required]
  -c, --project_count  Project count (default 50)
  -h, --help           Show help                                       [boolean]
  -v, --version        Show version number                             [boolean]

Examples:
  bus-factor.js -l rust -c 50  Check bus factor of 50 projects fetched from
                               Github with language rust order by stars
```

# Example
```
node bus-factor.js -l rust -c 50

┌─────────┬────────────┬───────┬──────────────────┬────────────┐
│ (index) │  project   │ stars │       user       │ percentage │
├─────────┼────────────┼───────┼──────────────────┼────────────┤
│    0    │ 'ripgrep'  │ 29173 │   'BurntSushi'   │    0.89    │
│    1    │   'swc'    │ 19984 │      'kdy1'      │    0.77    │
│    2    │   'exa'    │ 16404 │     'ogham'      │    0.84    │
│    3    │  'Rocket'  │ 16149 │ 'SergioBenitez'  │    0.86    │
│    4    │ 'appflowy' │ 16010 │    'appflowy'    │    0.79    │
│    5    │   'iced'   │ 12690 │     'hecrj'      │    0.88    │
│    6    │  'sonic'   │ 12525 │ 'valeriansaliou' │    0.92    │
│    7    │  'delta'   │ 12076 │   'dandavison'   │    0.87    │
│    8    │   'navi'   │ 10221 │   'denisidoro'   │    0.76    │
│    9    │  'pyxel'   │ 9369  │     'kitao'      │    0.98    │
│   10    │  'hyper'   │ 9107  │  'seanmonstar'   │    0.79    │
│   11    │   'book'   │ 8579  │ 'carols10cents'  │    0.77    │
│   12    │   'zola'   │ 8064  │     'Keats'      │    0.76    │
│   13    │   'xsv'    │ 7927  │   'BurntSushi'   │    0.92    │
└─────────┴────────────┴───────┴──────────────────┴────────────┘
Total execution time: 2.88 sec.
```

# Tests
```shell
npm run test
npm run test:unit
npm run test:integration
npm run test:coverage
```

# Architecture Decisions

1. [Record architecture decisions](doc/architecture/decisions/0001-record-architecture-decisions.md)
2. [Language](doc/architecture/decisions/0002-language.md)
3. [OO Style](doc/architecture/decisions/0003-code-style.md)
4. [Third-party libraries](doc/architecture/decisions/0004-third-party-libraries.md)
5. [Test framework](doc/architecture/decisions/0005-test-framework.md)
