module.exports = (config) => {
  if (!config.min_threshold) {
    throw new Error("Min threshold is not defined in the configuration");
  }
  return {
    find: (projects) => {
      const results = [];
      for (const project of projects) {
        if (!project.contributors.length) continue;
        let mostActiveDev = project.contributors[0];
        let totalContributionsCountForMostActiveDevs = 0;
        for (const contributor of project.contributors) {
          // This may not be necessary, but I check in case the provider returns disordered contributors
          if (contributor.contributions > mostActiveDev.contributions) {
            mostActiveDev = contributor;
          }
          totalContributionsCountForMostActiveDevs += contributor.contributions;
        }
        const percentage = mostActiveDev.contributions / totalContributionsCountForMostActiveDevs;
        if (percentage >= config.min_threshold) {
          results.push({
            project: project.name,
            stars: project.stars,
            user: mostActiveDev.login,
            percentage: Math.round((percentage + Number.EPSILON) * 100) / 100,
          });
        }
      }
      return results;
    },
  };
};
