require("dotenv").config();

// if necessary, I can implement a more advanced config loader for specific environment
module.exports = {
  min_threshold: 0.75,
  providers: {
    github: {
      auth: process.env.GITHUB_TOKEN,
      sort_by: "stars",
    },
  },
};
