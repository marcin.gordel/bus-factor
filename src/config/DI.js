const BusFactorService = require("../services/BusFactorService");
const BusFactoryFinder = require("../domain/BusFactorFinder");
const GitHubProvider = require("../providers/GitHubProvider");
const Logger = require("../infrastructure/Logger");

/*
 * Primitive implementation of Dependency Injection Container
 */
module.exports = (config) => {
  const githubProvider = GitHubProvider(config);
  const busFactoryFinder = BusFactoryFinder(config);
  const logger = Logger();
  const busFactoryService = BusFactorService(githubProvider, busFactoryFinder, logger);
  return {
    busFactoryService,
    logger,
  };
};
