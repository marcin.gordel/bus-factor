module.exports = (provider, busFactorFinder, logger) => {
  return {
    search: async (language, projectCount) => {
      const projects = await provider.fetchProjects(language, projectCount);
      const projectsWithContributors = await Promise.all(
        projects.map((project) =>
          provider.fetchContributors(project).catch((error) => {
            logger.error(`Fetching contributors for project ${project.name} failed. ` + error.toString());
            return project;
          })
        )
      );
      const projectsWithContributorsSortedByStarsDesc = projectsWithContributors.sort((a, b) => (a.stars < b.stars ? 1 : -1));
      return busFactorFinder.find(projectsWithContributorsSortedByStarsDesc);
    },
  };
};
