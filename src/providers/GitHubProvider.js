const { Octokit } = require("@octokit/core");

module.exports = (config) => {
  if (!config.providers?.github?.auth) {
    throw new Error("Github token is not defined in the configuration");
  }
  const octokit = new Octokit(config.providers.github);
  return {
    fetchProjects: async (language, count) => {
      if (count > 1000) {
        throw new Error("GitHub Search API provides up to 1,000 results for each search. Try to use a smaller one.");
      }
      const responseRateLimit = await octokit.request("GET /rate_limit");
      let pageCount = Math.ceil(count / 100);
      const searchLimitRemaining = responseRateLimit.data.resources.search.remaining;
      const coreLimitRemaining = responseRateLimit.data.resources.core.remaining;
      if (searchLimitRemaining === 0 || coreLimitRemaining === 0) {
        const resetTime = new Date((coreLimitRemaining > searchLimitRemaining ? coreLimitRemaining : searchLimitRemaining) * 1000);
        throw new Error("You have currently used up your github rate limit. The limit will reset at " + resetTime.toLocaleTimeString());
      }
      if (pageCount > responseRateLimit.data.resources.search.remaining || count > responseRateLimit.data.resources.core.remaining) {
        throw new Error("Project count is greater than your remaining GitHub Search Api rate limit. Try to use a smaller one.");
      }
      const perPage = count > 100 ? 100 : count;
      const sort = config.providers.github.sort_by || "stars";
      const projects = [];
      for (let page = 1; page <= pageCount; page++) {
        const response = await octokit.request("GET /search/repositories", {
          q: `language:${language}`,
          sort: sort,
          per_page: perPage,
          page,
        });
        if (response.status !== 200) throw new Error(response.data);
        projects.push(
          ...response.data.items.map((item) => ({
            name: item.name,
            stars: item.stargazers_count,
            owner: {
              login: item.owner.login,
            },
            repo: item.name,
          }))
        );
        if (projects.length >= count) break;
      }
      return projects;
    },
    fetchContributors: async (project) => {
      /* From GitHUb docs:
      Lists contributors to the specified repository and sorts them by the number of commits
      per contributor in descending order. This endpoint may return information that is a few hours old
      because the GitHub REST API v3 caches contributor data to improve performance.
       */
      const response = await octokit.request("GET /repos/{owner}/{repo}/contributors", {
        owner: project.owner.login,
        repo: project.repo,
        per_page: 25,
        page: 1,
      });
      if (response.status !== 200) {
        throw new Error(response.data);
      }
      return {
        name: project.name,
        stars: project.stars,
        contributors: response.data.map((item) => ({
          login: item.login,
          contributions: item.contributions,
        })),
      };
    },
  };
};
