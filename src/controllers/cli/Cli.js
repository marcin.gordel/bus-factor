const config = require("../../config/Config");
const DI = require("../../config/DI")(config);
const yargs = require("yargs/yargs");
const { hideBin } = require("yargs/helpers");

const argv = yargs(hideBin(process.argv))
  .usage("Usage: $0 --language [string] --project_count [number]")
  .example("$0 -l rust -c 50", "Check bus factor of 50 projects fetched from Github with language rust order by stars")
  .alias("l", "language")
  .alias("c", "project_count")
  .describe("l", "Programming language")
  .describe("c", "Project count (default 50)")
  .demandOption(["l"])
  .help("h")
  .alias("h", "help")
  .alias("v", "version").argv;

const timeStart = new Date().valueOf();
const logger = DI.logger;
DI.busFactoryService
  .search(argv.language, argv.project_count || 50)
  .then((results) => {
    console.table(results);
    const timeStop = new Date().valueOf();
    logger.log("Total execution time:", ((timeStop - timeStart) / 1000).toFixed(2), "sec.");
  })
  .catch((error) => {
    logger.error(error.toString());
  });
