module.exports = {
  items: [
    {
      name: "deno",
      stars: 80018,
      stargazers_count: 80018,
      owner: {
        login: "denoland",
      },
      repo: "deno",
      contributors: [
        {
          login: "ry",
          contributions: 1384,
        },
        {
          login: "bartlomieju",
          contributions: 862,
        },
        {
          login: "piscisaureus",
          contributions: 413,
        },
        {
          login: "kitsonk",
          contributions: 406,
        },
        {
          login: "caspervonb",
          contributions: 321,
        },
        {
          login: "lucacasonato",
          contributions: 290,
        },
        {
          login: "nayeemrmn",
          contributions: 226,
        },
        {
          login: "kt3k",
          contributions: 175,
        },
        {
          login: "AaronO",
          contributions: 169,
        },
        {
          login: "kevinkassimo",
          contributions: 167,
        },
        {
          login: "dsherret",
          contributions: 152,
        },
        {
          login: "crowlKats",
          contributions: 108,
        },
        {
          login: "littledivy",
          contributions: 92,
        },
        {
          login: "justjavac",
          contributions: 80,
        },
        {
          login: "marcosc90",
          contributions: 73,
        },
        {
          login: "zekth",
          contributions: 60,
        },
        {
          login: "andreubotella",
          contributions: 59,
        },
        {
          login: "cknight",
          contributions: 58,
        },
        {
          login: "axetroy",
          contributions: 56,
        },
        {
          login: "magurotuna",
          contributions: 54,
        },
        {
          login: "satyarohith",
          contributions: 43,
        },
        {
          login: "bnoordhuis",
          contributions: 40,
        },
        {
          login: "keroxp",
          contributions: 40,
        },
        {
          login: "afinch7",
          contributions: 40,
        },
        {
          login: "uki00a",
          contributions: 36,
        },
      ],
    },
    {
      name: "rust",
      stars: 62818,
      stargazers_count: 62818,

      owner: {
        login: "rust-lang",
      },
      repo: "rust",
      contributors: [
        {
          login: "bors",
          contributions: 22832,
        },
        {
          login: "brson",
          contributions: 5507,
        },
        {
          login: "alexcrichton",
          contributions: 5073,
        },
        {
          login: "Centril",
          contributions: 4223,
        },
        {
          login: "nikomatsakis",
          contributions: 4130,
        },
        {
          login: "GuillaumeGomez",
          contributions: 3656,
        },
        {
          login: "Manishearth",
          contributions: 3571,
        },
        {
          login: "pcwalton",
          contributions: 2829,
        },
        {
          login: "RalfJung",
          contributions: 2667,
        },
        {
          login: "JohnTitor",
          contributions: 2512,
        },
        {
          login: "bjorn3",
          contributions: 2477,
        },
        {
          login: "graydon",
          contributions: 2229,
        },
        {
          login: "nrc",
          contributions: 2177,
        },
        {
          login: "Dylan-DPC",
          contributions: 1913,
        },
        {
          login: "eddyb",
          contributions: 1861,
        },
        {
          login: "kennytm",
          contributions: 1839,
        },
        {
          login: "steveklabnik",
          contributions: 1795,
        },
        {
          login: "estebank",
          contributions: 1783,
        },
        {
          login: "matthiaskrgr",
          contributions: 1674,
        },
        {
          login: "petrochenkov",
          contributions: 1651,
        },
        {
          login: "topecongiro",
          contributions: 1614,
        },
        {
          login: "Mark-Simulacrum",
          contributions: 1536,
        },
        {
          login: "catamorphism",
          contributions: 1523,
        },
        {
          login: "pnkfelix",
          contributions: 1243,
        },
        {
          login: "frewsxcv",
          contributions: 1132,
        },
      ],
    },
    {
      name: "alacritty",
      stars: 37140,
      stargazers_count: 37140,

      owner: {
        login: "alacritty",
      },
      repo: "alacritty",
      contributors: [
        {
          login: "chrisduerr",
          contributions: 631,
        },
        {
          login: "jwilm",
          contributions: 496,
        },
        {
          login: "kchibisov",
          contributions: 152,
        },
        {
          login: "matthiaskrgr",
          contributions: 23,
        },
        {
          login: "nixpulvis",
          contributions: 23,
        },
        {
          login: "davidhewitt",
          contributions: 20,
        },
        {
          login: "siiptuo",
          contributions: 12,
        },
        {
          login: "zacps",
          contributions: 11,
        },
        {
          login: "Aaron1011",
          contributions: 10,
        },
        {
          login: "NickeZ",
          contributions: 10,
        },
        {
          login: "a5ob7r",
          contributions: 10,
        },
        {
          login: "mbrumlow",
          contributions: 9,
        },
        {
          login: "Eijebong",
          contributions: 8,
        },
        {
          login: "algesten",
          contributions: 8,
        },
        {
          login: "atouchet",
          contributions: 7,
        },
        {
          login: "hlieberman",
          contributions: 7,
        },
        {
          login: "sodiumjoe",
          contributions: 7,
        },
        {
          login: "Manishearth",
          contributions: 7,
        },
        {
          login: "robertgzr",
          contributions: 7,
        },
        {
          login: "cema-sp",
          contributions: 6,
        },
        {
          login: "lukaslueg",
          contributions: 6,
        },
        {
          login: "DivineGod",
          contributions: 5,
        },
        {
          login: "DSpeckhals",
          contributions: 5,
        },
        {
          login: "CrackedP0t",
          contributions: 5,
        },
        {
          login: "chetgurevitch",
          contributions: 5,
        },
      ],
    },
    {
      name: "bat",
      stars: 31902,
      stargazers_count: 31902,

      owner: {
        login: "sharkdp",
      },
      repo: "bat",
      contributors: [
        {
          login: "sharkdp",
          contributions: 909,
        },
        {
          login: "Enselic",
          contributions: 179,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 162,
        },
        {
          login: "eth-p",
          contributions: 147,
        },
        {
          login: "dependabot[bot]",
          contributions: 49,
        },
        {
          login: "mohamed-abdelnour",
          contributions: 35,
        },
        {
          login: "keith-hall",
          contributions: 32,
        },
        {
          login: "DuckerMan",
          contributions: 28,
        },
        {
          login: "BrainMaestro",
          contributions: 22,
        },
        {
          login: "DrSensor",
          contributions: 18,
        },
        {
          login: "henil",
          contributions: 16,
        },
        {
          login: "Kienyew",
          contributions: 16,
        },
        {
          login: "scop",
          contributions: 15,
        },
        {
          login: "neuronull",
          contributions: 14,
        },
        {
          login: "chris48s",
          contributions: 14,
        },
        {
          login: "niklasmohrin",
          contributions: 13,
        },
        {
          login: "rivy",
          contributions: 12,
        },
        {
          login: "ms2300",
          contributions: 12,
        },
        {
          login: "connorkuehl",
          contributions: 11,
        },
        {
          login: "shmokmt",
          contributions: 10,
        },
        {
          login: "LordFlashmeow",
          contributions: 10,
        },
        {
          login: "bojan88",
          contributions: 8,
        },
        {
          login: "loganintech",
          contributions: 7,
        },
        {
          login: "adrian-rivera",
          contributions: 6,
        },
        {
          login: "xeruf",
          contributions: 6,
        },
      ],
    },
    {
      name: "tauri",
      stars: 29639,
      stargazers_count: 29639,

      owner: {
        login: "tauri-apps",
      },
      repo: "tauri",
      contributors: [
        {
          login: "lucasfernog",
          contributions: 685,
        },
        {
          login: "renovate[bot]",
          contributions: 480,
        },
        {
          login: "jbolda",
          contributions: 123,
        },
        {
          login: "nothingismagick",
          contributions: 83,
        },
        {
          login: "amrbashir",
          contributions: 70,
        },
        {
          login: "tensor-programming",
          contributions: 60,
        },
        {
          login: "chippers",
          contributions: 58,
        },
        {
          login: "github-actions[bot]",
          contributions: 55,
        },
        {
          login: "nklayman",
          contributions: 44,
        },
        {
          login: "rajivshah3",
          contributions: 29,
        },
        {
          login: "lemarier",
          contributions: 29,
        },
        {
          login: "wusyong",
          contributions: 26,
        },
        {
          login: "dependabot[bot]",
          contributions: 16,
        },
        {
          login: "FabianLars",
          contributions: 15,
        },
        {
          login: "JonasKruckenberg",
          contributions: 9,
        },
        {
          login: "probablykasper",
          contributions: 9,
        },
        {
          login: "thadguidry",
          contributions: 7,
        },
        {
          login: "Laegel",
          contributions: 5,
        },
        {
          login: "BenoitRanque",
          contributions: 4,
        },
        {
          login: "lorenzolewis",
          contributions: 4,
        },
        {
          login: "rstoenescu",
          contributions: 4,
        },
        {
          login: "zakpatterson",
          contributions: 4,
        },
        {
          login: "xuchaoqian",
          contributions: 3,
        },
        {
          login: "diit",
          contributions: 3,
        },
        {
          login: "RealDrPuc",
          contributions: 3,
        },
      ],
    },
    {
      name: "ripgrep",
      stars: 29138,
      stargazers_count: 29138,

      owner: {
        login: "BurntSushi",
      },
      repo: "ripgrep",
      contributors: [
        {
          login: "BurntSushi",
          contributions: 1158,
        },
        {
          login: "okdana",
          contributions: 43,
        },
        {
          login: "balajisivaraman",
          contributions: 10,
        },
        {
          login: "tiehuis",
          contributions: 9,
        },
        {
          login: "ericbn",
          contributions: 7,
        },
        {
          login: "lyuha",
          contributions: 7,
        },
        {
          login: "theamazingfedex",
          contributions: 6,
        },
        {
          login: "jgarte",
          contributions: 6,
        },
        {
          login: "ignatenkobrain",
          contributions: 5,
        },
        {
          login: "kpp",
          contributions: 5,
        },
        {
          login: "epage",
          contributions: 4,
        },
        {
          login: "matthiaskrgr",
          contributions: 4,
        },
        {
          login: "sebnow",
          contributions: 4,
        },
        {
          login: "chocolateboy",
          contributions: 4,
        },
        {
          login: "SimenB",
          contributions: 4,
        },
        {
          login: "moshen",
          contributions: 3,
        },
        {
          login: "mernen",
          contributions: 3,
        },
        {
          login: "lilianmoraru",
          contributions: 3,
        },
        {
          login: "martinlindhe",
          contributions: 3,
        },
        {
          login: "behnam",
          contributions: 3,
        },
        {
          login: "sharkdp",
          contributions: 3,
        },
        {
          login: "llogiq",
          contributions: 3,
        },
        {
          login: "pierreN",
          contributions: 3,
        },
        {
          login: "durka",
          contributions: 3,
        },
        {
          login: "alexlafroscia",
          contributions: 2,
        },
      ],
    },
    {
      name: "awesome-rust",
      stars: 23921,
      stargazers_count: 23921,

      owner: {
        login: "rust-unofficial",
      },
      repo: "awesome-rust",
      contributors: [
        {
          login: "palfrey",
          contributions: 587,
        },
        {
          login: "luciusmagn",
          contributions: 495,
        },
        {
          login: "kud1ing",
          contributions: 296,
        },
        {
          login: "vityafx",
          contributions: 19,
        },
        {
          login: "BinChengZhao",
          contributions: 16,
        },
        {
          login: "williamdes",
          contributions: 13,
        },
        {
          login: "usagi",
          contributions: 11,
        },
        {
          login: "mmstick",
          contributions: 11,
        },
        {
          login: "YoshiTheChinchilla",
          contributions: 10,
        },
        {
          login: "ivanceras",
          contributions: 10,
        },
        {
          login: "chrislearn",
          contributions: 8,
        },
        {
          login: "frewsxcv",
          contributions: 8,
        },
        {
          login: "booyaa",
          contributions: 8,
        },
        {
          login: "b23r0",
          contributions: 8,
        },
        {
          login: "yaa110",
          contributions: 8,
        },
        {
          login: "svenstaro",
          contributions: 7,
        },
        {
          login: "flyq",
          contributions: 7,
        },
        {
          login: "danielpclark",
          contributions: 7,
        },
        {
          login: "kbknapp",
          contributions: 7,
        },
        {
          login: "ManevilleF",
          contributions: 7,
        },
        {
          login: "naomijub",
          contributions: 7,
        },
        {
          login: "vamolessa",
          contributions: 7,
        },
        {
          login: "Oscuro87",
          contributions: 7,
        },
        {
          login: "whitfin",
          contributions: 6,
        },
        {
          login: "yaahc",
          contributions: 6,
        },
      ],
    },
    {
      name: "starship",
      stars: 21964,
      stargazers_count: 21964,

      owner: {
        login: "starship",
      },
      repo: "starship",
      contributors: [
        {
          login: "dependabot-preview[bot]",
          contributions: 283,
        },
        {
          login: "matchai",
          contributions: 210,
        },
        {
          login: "dependabot[bot]",
          contributions: 151,
        },
        {
          login: "davidkna",
          contributions: 123,
        },
        {
          login: "andytom",
          contributions: 119,
        },
        {
          login: "allcontributors[bot]",
          contributions: 83,
        },
        {
          login: "vladimyr",
          contributions: 44,
        },
        {
          login: "chipbuster",
          contributions: 43,
        },
        {
          login: "cappyzawa",
          contributions: 28,
        },
        {
          login: "heyrict",
          contributions: 26,
        },
        {
          login: "HansAuger",
          contributions: 14,
        },
        {
          login: "ATiltedTree",
          contributions: 12,
        },
        {
          login: "kidonng",
          contributions: 11,
        },
        {
          login: "wyze",
          contributions: 9,
        },
        {
          login: "alexmaco",
          contributions: 8,
        },
        {
          login: "bbigras",
          contributions: 8,
        },
        {
          login: "cymruu",
          contributions: 8,
        },
        {
          login: "jletey",
          contributions: 8,
        },
        {
          login: "Scotsguy",
          contributions: 6,
        },
        {
          login: "nickwb",
          contributions: 6,
        },
        {
          login: "tiffanosaurus",
          contributions: 6,
        },
        {
          login: "ahouts",
          contributions: 5,
        },
        {
          login: "jonstodle",
          contributions: 5,
        },
        {
          login: "qmatias",
          contributions: 5,
        },
        {
          login: "m0nhawk",
          contributions: 4,
        },
      ],
    },
    {
      name: "rustlings",
      stars: 21758,
      stargazers_count: 21758,

      owner: {
        login: "rust-lang",
      },
      repo: "rustlings",
      contributors: [
        {
          login: "diannasoreil",
          contributions: 218,
        },
        {
          login: "allcontributors[bot]",
          contributions: 173,
        },
        {
          login: "carols10cents",
          contributions: 83,
        },
        {
          login: "bors",
          contributions: 54,
        },
        {
          login: "komaeda",
          contributions: 51,
        },
        {
          login: "AbdouSeck",
          contributions: 16,
        },
        {
          login: "tlyu",
          contributions: 13,
        },
        {
          login: "sjmann",
          contributions: 13,
        },
        {
          login: "alexxroche",
          contributions: 9,
        },
        {
          login: "jrvidal",
          contributions: 9,
        },
        {
          login: "Zerotask",
          contributions: 9,
        },
        {
          login: "cjpearce",
          contributions: 8,
        },
        {
          login: "apogeeoak",
          contributions: 8,
        },
        {
          login: "sanjaykdragon",
          contributions: 8,
        },
        {
          login: "danwilhelm",
          contributions: 7,
        },
        {
          login: "eddyp",
          contributions: 5,
        },
        {
          login: "nkanderson",
          contributions: 5,
        },
        {
          login: "abagshaw",
          contributions: 5,
        },
        {
          login: "evestera",
          contributions: 4,
        },
        {
          login: "miller-time",
          contributions: 4,
        },
        {
          login: "ryanpcmcquen",
          contributions: 4,
        },
        {
          login: "kolbma",
          contributions: 4,
        },
        {
          login: "Delet0r",
          contributions: 4,
        },
        {
          login: "seeplusplus",
          contributions: 4,
        },
        {
          login: "juanprq",
          contributions: 3,
        },
      ],
    },
    {
      name: "meilisearch",
      stars: 21037,
      stargazers_count: 21037,

      owner: {
        login: "meilisearch",
      },
      repo: "meilisearch",
      contributors: [
        {
          login: "Kerollmops",
          contributions: 1230,
        },
        {
          login: "MarinPostma",
          contributions: 726,
        },
        {
          login: "bors[bot]",
          contributions: 376,
        },
        {
          login: "qdequele",
          contributions: 237,
        },
        {
          login: "curquiza",
          contributions: 209,
        },
        {
          login: "irevoire",
          contributions: 195,
        },
        {
          login: "ManyTheFish",
          contributions: 71,
        },
        {
          login: "tpayet",
          contributions: 27,
        },
        {
          login: "matboivin",
          contributions: 27,
        },
        {
          login: "shekhirin",
          contributions: 14,
        },
        {
          login: "bidoubiwa",
          contributions: 14,
        },
        {
          login: "ppamorim",
          contributions: 13,
        },
        {
          login: "gorogoroumaru",
          contributions: 11,
        },
        {
          login: "balajisivaraman",
          contributions: 10,
        },
        {
          login: "erlend-sh",
          contributions: 7,
        },
        {
          login: "patrickdung",
          contributions: 7,
        },
        {
          login: "fharper",
          contributions: 6,
        },
        {
          login: "gmourier",
          contributions: 6,
        },
        {
          login: "CaroFG",
          contributions: 6,
        },
        {
          login: "emresaglam",
          contributions: 5,
        },
        {
          login: "sanders41",
          contributions: 5,
        },
        {
          login: "mdubus",
          contributions: 5,
        },
        {
          login: "djKooks",
          contributions: 4,
        },
        {
          login: "meili-bot",
          contributions: 4,
        },
        {
          login: "sgummaluri",
          contributions: 4,
        },
      ],
    },
    {
      name: "fd",
      stars: 20416,
      stargazers_count: 20416,

      owner: {
        login: "sharkdp",
      },
      repo: "fd",
      contributors: [
        {
          login: "sharkdp",
          contributions: 500,
        },
        {
          login: "tmccombs",
          contributions: 28,
        },
        {
          login: "reima",
          contributions: 22,
        },
        {
          login: "alexmaco",
          contributions: 21,
        },
        {
          login: "tavianator",
          contributions: 21,
        },
        {
          login: "dependabot[bot]",
          contributions: 18,
        },
        {
          login: "jcaplan",
          contributions: 16,
        },
        {
          login: "marionebl",
          contributions: 14,
        },
        {
          login: "mmstick",
          contributions: 10,
        },
        {
          login: "FallenWarrior2k",
          contributions: 10,
        },
        {
          login: "DJRHails",
          contributions: 9,
        },
        {
          login: "fusillicode",
          contributions: 8,
        },
        {
          login: "mookid",
          contributions: 6,
        },
        {
          login: "niklasmohrin",
          contributions: 6,
        },
        {
          login: "matematikaadit",
          contributions: 5,
        },
        {
          login: "Detegr",
          contributions: 5,
        },
        {
          login: "PramodBisht",
          contributions: 5,
        },
        {
          login: "Doxterpepper",
          contributions: 5,
        },
        {
          login: "stevepentland",
          contributions: 5,
        },
        {
          login: "detly",
          contributions: 4,
        },
        {
          login: "aswild",
          contributions: 3,
        },
        {
          login: "stephane-archer",
          contributions: 3,
        },
        {
          login: "JonathanxD",
          contributions: 3,
        },
        {
          login: "maxtriano",
          contributions: 3,
        },
        {
          login: "pablopunk",
          contributions: 3,
        },
      ],
    },
    {
      name: "swc",
      stars: 19912,
      stargazers_count: 19912,

      owner: {
        login: "swc-project",
      },
      repo: "swc",
      contributors: [
        {
          login: "kdy1",
          contributions: 1680,
        },
        {
          login: "alexander-akait",
          contributions: 79,
        },
        {
          login: "dsherret",
          contributions: 76,
        },
        {
          login: "kwonoj",
          contributions: 62,
        },
        {
          login: "g-plane",
          contributions: 38,
        },
        {
          login: "Brooooooklyn",
          contributions: 35,
        },
        {
          login: "sosukesuzuki",
          contributions: 25,
        },
        {
          login: "magic-akari",
          contributions: 25,
        },
        {
          login: "bors[bot]",
          contributions: 24,
        },
        {
          login: "Austaras",
          contributions: 21,
        },
        {
          login: "RiESAEX",
          contributions: 18,
        },
        {
          login: "devongovett",
          contributions: 15,
        },
        {
          login: "nayeemrmn",
          contributions: 11,
        },
        {
          login: "bartlomieju",
          contributions: 9,
        },
        {
          login: "IronLu233",
          contributions: 8,
        },
        {
          login: "tmpfs",
          contributions: 7,
        },
        {
          login: "mischnic",
          contributions: 6,
        },
        {
          login: "braddunbar",
          contributions: 5,
        },
        {
          login: "punkeel",
          contributions: 5,
        },
        {
          login: "Shinyaigeek",
          contributions: 4,
        },
        {
          login: "iheyunfei",
          contributions: 4,
        },
        {
          login: "dependabot[bot]",
          contributions: 4,
        },
        {
          login: "erikdesjardins",
          contributions: 4,
        },
        {
          login: "fabiosantoscode",
          contributions: 3,
        },
        {
          login: "95th",
          contributions: 3,
        },
      ],
    },
    {
      name: "xi-editor",
      stars: 18980,
      stargazers_count: 18980,

      owner: {
        login: "xi-editor",
      },
      repo: "xi-editor",
      contributors: [
        {
          login: "cmyr",
          contributions: 583,
        },
        {
          login: "raphlinus",
          contributions: 403,
        },
        {
          login: "scholtzan",
          contributions: 188,
        },
        {
          login: "trishume",
          contributions: 124,
        },
        {
          login: "mbStavola",
          contributions: 55,
        },
        {
          login: "dtolnay",
          contributions: 43,
        },
        {
          login: "nangtrongvuon",
          contributions: 41,
        },
        {
          login: "06kellyjac",
          contributions: 39,
        },
        {
          login: "betterclever",
          contributions: 34,
        },
        {
          login: "arazabishov",
          contributions: 33,
        },
        {
          login: "mqzry",
          contributions: 22,
        },
        {
          login: "TDecki",
          contributions: 21,
        },
        {
          login: "Cogitri",
          contributions: 19,
        },
        {
          login: "jmuk",
          contributions: 17,
        },
        {
          login: "YangKeao",
          contributions: 17,
        },
        {
          login: "ahouts",
          contributions: 17,
        },
        {
          login: "rkusa",
          contributions: 16,
        },
        {
          login: "ineol",
          contributions: 15,
        },
        {
          login: "virattara",
          contributions: 11,
        },
        {
          login: "LiHRaM",
          contributions: 11,
        },
        {
          login: "llogiq",
          contributions: 10,
        },
        {
          login: "terhechte",
          contributions: 9,
        },
        {
          login: "timpbrenner",
          contributions: 9,
        },
        {
          login: "akxcv",
          contributions: 8,
        },
        {
          login: "eyelash",
          contributions: 8,
        },
      ],
    },
    {
      name: "yew",
      stars: 18862,
      stargazers_count: 18862,

      owner: {
        login: "yewstack",
      },
      repo: "yew",
      contributors: [
        {
          login: "DenisKolodin",
          contributions: 465,
        },
        {
          login: "jstarry",
          contributions: 442,
        },
        {
          login: "bors[bot]",
          contributions: 159,
        },
        {
          login: "teymour-aldridge",
          contributions: 56,
        },
        {
          login: "hgzimmerman",
          contributions: 56,
        },
        {
          login: "siku2",
          contributions: 51,
        },
        {
          login: "hamza1311",
          contributions: 50,
        },
        {
          login: "voidpumpkin",
          contributions: 48,
        },
        {
          login: "mc1098",
          contributions: 46,
        },
        {
          login: "lili668668",
          contributions: 35,
        },
        {
          login: "gitlocalize-app[bot]",
          contributions: 29,
        },
        {
          login: "dependabot[bot]",
          contributions: 21,
        },
        {
          login: "futursolo",
          contributions: 19,
        },
        {
          login: "cecton",
          contributions: 14,
        },
        {
          login: "Yew-Temp-Bot",
          contributions: 13,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 13,
        },
        {
          login: "Madoshakalaka",
          contributions: 12,
        },
        {
          login: "kellytk",
          contributions: 12,
        },
        {
          login: "Xavientois",
          contributions: 11,
        },
        {
          login: "bakape",
          contributions: 11,
        },
        {
          login: "tafia",
          contributions: 9,
        },
        {
          login: "Stigjb",
          contributions: 9,
        },
        {
          login: "astraw",
          contributions: 7,
        },
        {
          login: "philip-peterson",
          contributions: 7,
        },
        {
          login: "totorigolo",
          contributions: 7,
        },
      ],
    },
    {
      name: "firecracker",
      stars: 17095,
      stargazers_count: 17095,

      owner: {
        login: "firecracker-microvm",
      },
      repo: "firecracker",
      contributors: [
        {
          login: "dianpopa",
          contributions: 493,
        },
        {
          login: "acatangiu",
          contributions: 417,
        },
        {
          login: "andreeaflorescu",
          contributions: 314,
        },
        {
          login: "serban300",
          contributions: 262,
        },
        {
          login: "alindima",
          contributions: 230,
        },
        {
          login: "alexandruag",
          contributions: 185,
        },
        {
          login: "aghecenco",
          contributions: 181,
        },
        {
          login: "georgepisaltu",
          contributions: 154,
        },
        {
          login: "luminitavoicu",
          contributions: 111,
        },
        {
          login: "sandreim",
          contributions: 107,
        },
        {
          login: "dhrgit",
          contributions: 84,
        },
        {
          login: "AlexandruCihodaru",
          contributions: 75,
        },
        {
          login: "raduweiss",
          contributions: 51,
        },
        {
          login: "lauralt",
          contributions: 41,
        },
        {
          login: "ioanachirca",
          contributions: 32,
        },
        {
          login: "fristonio",
          contributions: 17,
        },
        {
          login: "tamionv",
          contributions: 17,
        },
        {
          login: "jiangliu",
          contributions: 15,
        },
        {
          login: "raduiliescu",
          contributions: 15,
        },
        {
          login: "KarthikNedunchezhiyan",
          contributions: 15,
        },
        {
          login: "damienstanton",
          contributions: 12,
        },
        {
          login: "alsrdn",
          contributions: 10,
        },
        {
          login: "kzys",
          contributions: 10,
        },
        {
          login: "xiekeyang",
          contributions: 10,
        },
        {
          login: "alexbranciog",
          contributions: 9,
        },
      ],
    },
    {
      name: "nushell",
      stars: 17093,
      stargazers_count: 17093,

      owner: {
        login: "nushell",
      },
      repo: "nushell",
      contributors: [
        {
          login: "jntrnr",
          contributions: 470,
        },
        {
          login: "andrasio",
          contributions: 382,
        },
        {
          login: "fdncred",
          contributions: 244,
        },
        {
          login: "wycats",
          contributions: 165,
        },
        {
          login: "JosephTLyons",
          contributions: 88,
        },
        {
          login: "thegedge",
          contributions: 67,
        },
        {
          login: "est31",
          contributions: 52,
        },
        {
          login: "gillespiecd",
          contributions: 44,
        },
        {
          login: "elferherrera",
          contributions: 42,
        },
        {
          login: "coolshaurya",
          contributions: 39,
        },
        {
          login: "LhKipp",
          contributions: 33,
        },
        {
          login: "kubouch",
          contributions: 32,
        },
        {
          login: "sebastian-xyz",
          contributions: 26,
        },
        {
          login: "DrSensor",
          contributions: 23,
        },
        {
          login: "notryanb",
          contributions: 22,
        },
        {
          login: "JonnyWalker81",
          contributions: 18,
        },
        {
          login: "luccasmmg",
          contributions: 18,
        },
        {
          login: "twe4ked",
          contributions: 18,
        },
        {
          login: "Amanita-muscaria",
          contributions: 15,
        },
        {
          login: "vsoch",
          contributions: 15,
        },
        {
          login: "quebin31",
          contributions: 15,
        },
        {
          login: "pka",
          contributions: 14,
        },
        {
          login: "ilius",
          contributions: 14,
        },
        {
          login: "stormasm",
          contributions: 13,
        },
        {
          login: "efx",
          contributions: 12,
        },
      ],
    },
    {
      name: "tools",
      stars: 17058,
      stargazers_count: 17058,

      owner: {
        login: "rome",
      },
      repo: "tools",
      contributors: [
        {
          login: "sebmck",
          contributions: 733,
        },
        {
          login: "ematipico",
          contributions: 217,
        },
        {
          login: "MichaReiser",
          contributions: 103,
        },
        {
          login: "yeonjuan",
          contributions: 82,
        },
        {
          login: "diokey",
          contributions: 82,
        },
        {
          login: "yassere",
          contributions: 82,
        },
        {
          login: "EduardoLopes",
          contributions: 76,
        },
        {
          login: "ooflorent",
          contributions: 74,
        },
        {
          login: "jer3m01",
          contributions: 60,
        },
        {
          login: "bitpshr",
          contributions: 51,
        },
        {
          login: "macovedj",
          contributions: 46,
        },
        {
          login: "VictorHom",
          contributions: 43,
        },
        {
          login: "Kelbie",
          contributions: 26,
        },
        {
          login: "mattcompiles",
          contributions: 24,
        },
        {
          login: "xunilrj",
          contributions: 23,
        },
        {
          login: "Boshen",
          contributions: 23,
        },
        {
          login: "JustBeYou",
          contributions: 23,
        },
        {
          login: "jamiebuilds",
          contributions: 19,
        },
        {
          login: "1ntEgr8",
          contributions: 18,
        },
        {
          login: "iaravindreddyp",
          contributions: 16,
        },
        {
          login: "yangshun",
          contributions: 15,
        },
        {
          login: "zinyando",
          contributions: 15,
        },
        {
          login: "wgardiner",
          contributions: 10,
        },
        {
          login: "leops",
          contributions: 10,
        },
        {
          login: "ikeryo1182",
          contributions: 8,
        },
      ],
    },
    {
      name: "relay",
      stars: 16642,
      stargazers_count: 16642,

      owner: {
        login: "facebook",
      },
      repo: "relay",
      contributors: [
        {
          login: "kassens",
          contributions: 1593,
        },
        {
          login: "alunyov",
          contributions: 702,
        },
        {
          login: "josephsavona",
          contributions: 541,
        },
        {
          login: "tyao1",
          contributions: 515,
        },
        {
          login: "jstejada",
          contributions: 507,
        },
        {
          login: "rbalicki2",
          contributions: 356,
        },
        {
          login: "wincent",
          contributions: 254,
        },
        {
          login: "leebyron",
          contributions: 251,
        },
        {
          login: "yungsters",
          contributions: 244,
        },
        {
          login: "steveluscher",
          contributions: 196,
        },
        {
          login: "captbaritone",
          contributions: 138,
        },
        {
          login: "yuzhi",
          contributions: 120,
        },
        {
          login: "JenniferWang",
          contributions: 62,
        },
        {
          login: "devknoll",
          contributions: 61,
        },
        {
          login: "gabelevi",
          contributions: 53,
        },
        {
          login: "mjmahone",
          contributions: 51,
        },
        {
          login: "poteto",
          contributions: 50,
        },
        {
          login: "mofeiZ",
          contributions: 46,
        },
        {
          login: "cpojer",
          contributions: 39,
        },
        {
          login: "mvsampath",
          contributions: 37,
        },
        {
          login: "dependabot[bot]",
          contributions: 36,
        },
        {
          login: "mroch",
          contributions: 26,
        },
        {
          login: "ginfung",
          contributions: 24,
        },
        {
          login: "robrichard",
          contributions: 23,
        },
        {
          login: "rubennorte",
          contributions: 23,
        },
      ],
    },
    {
      name: "exa",
      stars: 16367,
      stargazers_count: 16367,

      owner: {
        login: "ogham",
      },
      repo: "exa",
      contributors: [
        {
          login: "ogham",
          contributions: 1156,
        },
        {
          login: "ariasuni",
          contributions: 80,
        },
        {
          login: "nwin",
          contributions: 14,
        },
        {
          login: "skade",
          contributions: 12,
        },
        {
          login: "asoderman",
          contributions: 10,
        },
        {
          login: "maandree",
          contributions: 10,
        },
        {
          login: "spk",
          contributions: 9,
        },
        {
          login: "lilyball",
          contributions: 8,
        },
        {
          login: "daniellockyer",
          contributions: 7,
        },
        {
          login: "ignatenkobrain",
          contributions: 7,
        },
        {
          login: "PatriotRossii",
          contributions: 5,
        },
        {
          login: "Freaky",
          contributions: 5,
        },
        {
          login: "b05902132",
          contributions: 5,
        },
        {
          login: "grigorii-horos",
          contributions: 4,
        },
        {
          login: "mqudsi",
          contributions: 4,
        },
        {
          login: "mneumann",
          contributions: 4,
        },
        {
          login: "0rvar",
          contributions: 4,
        },
        {
          login: "killercup",
          contributions: 4,
        },
        {
          login: "Stebalien",
          contributions: 4,
        },
        {
          login: "lovesegfault",
          contributions: 3,
        },
        {
          login: "cgzones",
          contributions: 3,
        },
        {
          login: "coyotebush",
          contributions: 3,
        },
        {
          login: "FliegendeWurst",
          contributions: 3,
        },
        {
          login: "jcrd",
          contributions: 3,
        },
        {
          login: "jbeich",
          contributions: 3,
        },
      ],
    },
    {
      name: "diem",
      stars: 16329,
      stargazers_count: 16329,

      owner: {
        login: "diem",
      },
      repo: "diem",
      contributors: [
        {
          login: "bmwill",
          contributions: 805,
        },
        {
          login: "davidiw",
          contributions: 581,
        },
        {
          login: "JoshLind",
          contributions: 536,
        },
        {
          login: "msmouse",
          contributions: 412,
        },
        {
          login: "meng-xu-cs",
          contributions: 362,
        },
        {
          login: "gregnazario",
          contributions: 346,
        },
        {
          login: "phlip9",
          contributions: 327,
        },
        {
          login: "dependabot[bot]",
          contributions: 324,
        },
        {
          login: "huitseeker",
          contributions: 316,
        },
        {
          login: "tzakian",
          contributions: 275,
        },
        {
          login: "tnowacki",
          contributions: 273,
        },
        {
          login: "wrwg",
          contributions: 271,
        },
        {
          login: "xli",
          contributions: 261,
        },
        {
          login: "sblackshear",
          contributions: 254,
        },
        {
          login: "rexhoffman",
          contributions: 244,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 239,
        },
        {
          login: "sunshowers",
          contributions: 238,
        },
        {
          login: "zekun000",
          contributions: 235,
        },
        {
          login: "move-team",
          contributions: 180,
        },
        {
          login: "wqfish",
          contributions: 179,
        },
        {
          login: "mimoo",
          contributions: 154,
        },
        {
          login: "bob-wilson",
          contributions: 147,
        },
        {
          login: "lightmark",
          contributions: 147,
        },
        {
          login: "sausagee",
          contributions: 145,
        },
        {
          login: "ankushagarwal",
          contributions: 140,
        },
      ],
    },
    {
      name: "Rocket",
      stars: 16127,
      stargazers_count: 16127,

      owner: {
        login: "SergioBenitez",
      },
      repo: "Rocket",
      contributors: [
        {
          login: "SergioBenitez",
          contributions: 1506,
        },
        {
          login: "jebrosen",
          contributions: 170,
        },
        {
          login: "jhpratt",
          contributions: 15,
        },
        {
          login: "messense",
          contributions: 6,
        },
        {
          login: "ELD",
          contributions: 5,
        },
        {
          login: "sethlopezme",
          contributions: 4,
        },
        {
          login: "sunng87",
          contributions: 4,
        },
        {
          login: "the10thWiz",
          contributions: 3,
        },
        {
          login: "notriddle",
          contributions: 3,
        },
        {
          login: "mehcode",
          contributions: 3,
        },
        {
          login: "ThouCheese",
          contributions: 3,
        },
        {
          login: "illfygli",
          contributions: 2,
        },
        {
          login: "yungkneez",
          contributions: 2,
        },
        {
          login: "divyahansg",
          contributions: 2,
        },
        {
          login: "ianlet",
          contributions: 2,
        },
        {
          login: "karuna",
          contributions: 2,
        },
        {
          login: "ascclemens",
          contributions: 2,
        },
        {
          login: "lholden",
          contributions: 2,
        },
        {
          login: "LukasKalbertodt",
          contributions: 2,
        },
        {
          login: "mettke",
          contributions: 2,
        },
        {
          login: "paolobarbolini",
          contributions: 2,
        },
        {
          login: "Deedasmi",
          contributions: 2,
        },
        {
          login: "rbalicki2",
          contributions: 2,
        },
        {
          login: "sstangl",
          contributions: 2,
        },
        {
          login: "stuarth",
          contributions: 2,
        },
      ],
    },
    {
      name: "appflowy",
      stars: 15718,
      stargazers_count: 15718,

      owner: {
        login: "AppFlowy-IO",
      },
      repo: "appflowy",
      contributors: [
        {
          login: "appflowy",
          contributions: 828,
        },
        {
          login: "Minecodecraft",
          contributions: 54,
        },
        {
          login: "annieappflowy",
          contributions: 41,
        },
        {
          login: "MikeWallaceDev",
          contributions: 37,
        },
        {
          login: "stephengroat",
          contributions: 20,
        },
        {
          login: "Hari-07",
          contributions: 16,
        },
        {
          login: "pdckxd",
          contributions: 11,
        },
        {
          login: "rileyhawk1417",
          contributions: 4,
        },
        {
          login: "naughtz",
          contributions: 4,
        },
        {
          login: "irfanbacker",
          contributions: 3,
        },
        {
          login: "fabiograz",
          contributions: 3,
        },
        {
          login: "pratikbalar",
          contributions: 3,
        },
        {
          login: "slidoooor",
          contributions: 2,
        },
        {
          login: "Poly-Pixel",
          contributions: 2,
        },
        {
          login: "vnepogodin",
          contributions: 2,
        },
        {
          login: "ljshwyykl",
          contributions: 2,
        },
        {
          login: "notd5a-alt",
          contributions: 2,
        },
        {
          login: "akshay-rirev",
          contributions: 1,
        },
        {
          login: "factotvm",
          contributions: 1,
        },
        {
          login: "Discreater",
          contributions: 1,
        },
        {
          login: "Xoshbin",
          contributions: 1,
        },
        {
          login: "LeonardoRosaa",
          contributions: 1,
        },
        {
          login: "0xN0x",
          contributions: 1,
        },
        {
          login: "PabloCastellano",
          contributions: 1,
        },
        {
          login: "rayrrr",
          contributions: 1,
        },
      ],
    },
    {
      name: "tokio",
      stars: 14758,
      stargazers_count: 14758,

      owner: {
        login: "tokio-rs",
      },
      repo: "tokio",
      contributors: [
        {
          login: "carllerche",
          contributions: 500,
        },
        {
          login: "alexcrichton",
          contributions: 357,
        },
        {
          login: "Darksonn",
          contributions: 240,
        },
        {
          login: "taiki-e",
          contributions: 135,
        },
        {
          login: "ipetkov",
          contributions: 121,
        },
        {
          login: "hawkw",
          contributions: 88,
        },
        {
          login: "LucioFranco",
          contributions: 52,
        },
        {
          login: "seanmonstar",
          contributions: 51,
        },
        {
          login: "jonhoo",
          contributions: 38,
        },
        {
          login: "kpp",
          contributions: 29,
        },
        {
          login: "zaharidichev",
          contributions: 26,
        },
        {
          login: "asomers",
          contributions: 25,
        },
        {
          login: "vorot93",
          contributions: 23,
        },
        {
          login: "blasrodri",
          contributions: 22,
        },
        {
          login: "vorner",
          contributions: 20,
        },
        {
          login: "udoprog",
          contributions: 19,
        },
        {
          login: "Marwes",
          contributions: 16,
        },
        {
          login: "dekellum",
          contributions: 14,
        },
        {
          login: "sfackler",
          contributions: 14,
        },
        {
          login: "davidpdrsn",
          contributions: 13,
        },
        {
          login: "aturon",
          contributions: 12,
        },
        {
          login: "MikailBag",
          contributions: 12,
        },
        {
          login: "kleimkuhler",
          contributions: 12,
        },
        {
          login: "DoumanAsh",
          contributions: 11,
        },
        {
          login: "juleskers",
          contributions: 11,
        },
      ],
    },
    {
      name: "rustdesk",
      stars: 14680,
      stargazers_count: 14680,

      owner: {
        login: "rustdesk",
      },
      repo: "rustdesk",
      contributors: [
        {
          login: "rustdesk",
          contributions: 306,
        },
        {
          login: "open-trade",
          contributions: 40,
        },
        {
          login: "Heasn",
          contributions: 22,
        },
        {
          login: "fufesou",
          contributions: 14,
        },
        {
          login: "hatamiarash7",
          contributions: 7,
        },
        {
          login: "varghesejose2020",
          contributions: 7,
        },
        {
          login: "palfrey",
          contributions: 5,
        },
        {
          login: "cc-morning",
          contributions: 5,
        },
        {
          login: "SET19724",
          contributions: 3,
        },
        {
          login: "EmPablo",
          contributions: 2,
        },
        {
          login: "geshii",
          contributions: 2,
        },
        {
          login: "josephjosedev",
          contributions: 2,
        },
        {
          login: "starccy",
          contributions: 2,
        },
        {
          login: "Heap-Hop",
          contributions: 2,
        },
        {
          login: "dannkunt",
          contributions: 2,
        },
        {
          login: "desertstsung",
          contributions: 2,
        },
        {
          login: "ioridev",
          contributions: 2,
        },
        {
          login: "jeroenhd",
          contributions: 2,
        },
        {
          login: "mtoohey31",
          contributions: 2,
        },
        {
          login: "mingxingren",
          contributions: 2,
        },
        {
          login: "arthurhenrique",
          contributions: 1,
        },
        {
          login: "Candunc",
          contributions: 1,
        },
        {
          login: "herbygillot",
          contributions: 1,
        },
        {
          login: "gitatmax",
          contributions: 1,
        },
        {
          login: "sitiom",
          contributions: 1,
        },
      ],
    },
    {
      name: "vaultwarden",
      stars: 13909,
      stargazers_count: 13909,

      owner: {
        login: "dani-garcia",
      },
      repo: "vaultwarden",
      contributors: [
        {
          login: "dani-garcia",
          contributions: 979,
        },
        {
          login: "mprasil",
          contributions: 171,
        },
        {
          login: "BlackDex",
          contributions: 135,
        },
        {
          login: "jjlin",
          contributions: 115,
        },
        {
          login: "njfox",
          contributions: 35,
        },
        {
          login: "RealOrangeOne",
          contributions: 27,
        },
        {
          login: "vplme",
          contributions: 21,
        },
        {
          login: "ypid",
          contributions: 15,
        },
        {
          login: "janost",
          contributions: 15,
        },
        {
          login: "endyman",
          contributions: 13,
        },
        {
          login: "krankur",
          contributions: 13,
        },
        {
          login: "mqus",
          contributions: 11,
        },
        {
          login: "publicarray",
          contributions: 10,
        },
        {
          login: "Step7750",
          contributions: 10,
        },
        {
          login: "shauder",
          contributions: 10,
        },
        {
          login: "Skeen",
          contributions: 9,
        },
        {
          login: "olivierIllogika",
          contributions: 8,
        },
        {
          login: "fbartels",
          contributions: 6,
        },
        {
          login: "itr6",
          contributions: 6,
        },
        {
          login: "theycallmesteve",
          contributions: 6,
        },
        {
          login: "TheMardy",
          contributions: 6,
        },
        {
          login: "dongcarl",
          contributions: 4,
        },
        {
          login: "umireon",
          contributions: 4,
        },
        {
          login: "swedishborgie",
          contributions: 4,
        },
        {
          login: "laxmanpradhan",
          contributions: 4,
        },
      ],
    },
    {
      name: "bevy",
      stars: 13791,
      stargazers_count: 13791,

      owner: {
        login: "bevyengine",
      },
      repo: "bevy",
      contributors: [
        {
          login: "cart",
          contributions: 1079,
        },
        {
          login: "mockersf",
          contributions: 141,
        },
        {
          login: "superdump",
          contributions: 43,
        },
        {
          login: "jakobhellermann",
          contributions: 36,
        },
        {
          login: "DJMcNab",
          contributions: 28,
        },
        {
          login: "dependabot[bot]",
          contributions: 28,
        },
        {
          login: "TheRawMeatball",
          contributions: 27,
        },
        {
          login: "NathanSWard",
          contributions: 25,
        },
        {
          login: "MinerSebas",
          contributions: 24,
        },
        {
          login: "CleanCut",
          contributions: 22,
        },
        {
          login: "StarArawn",
          contributions: 21,
        },
        {
          login: "mfdorst",
          contributions: 20,
        },
        {
          login: "memoryruins",
          contributions: 19,
        },
        {
          login: "Davier",
          contributions: 18,
        },
        {
          login: "alice-i-cecile",
          contributions: 17,
        },
        {
          login: "bjorn3",
          contributions: 17,
        },
        {
          login: "zicklag",
          contributions: 15,
        },
        {
          login: "mrk-its",
          contributions: 14,
        },
        {
          login: "Ratysz",
          contributions: 14,
        },
        {
          login: "smokku",
          contributions: 14,
        },
        {
          login: "BoxyUwU",
          contributions: 13,
        },
        {
          login: "mtsr",
          contributions: 13,
        },
        {
          login: "NiklasEi",
          contributions: 13,
        },
        {
          login: "blunted2night",
          contributions: 10,
        },
        {
          login: "multun",
          contributions: 10,
        },
      ],
    },
    {
      name: "v86",
      stars: 13234,
      stargazers_count: 13234,

      owner: {
        login: "copy",
      },
      repo: "v86",
      contributors: [
        {
          login: "copy",
          contributions: 2626,
        },
        {
          login: "AmaanC",
          contributions: 692,
        },
        {
          login: "ErnWong",
          contributions: 347,
        },
        {
          login: "awalgarg",
          contributions: 136,
        },
        {
          login: "davidbullado",
          contributions: 22,
        },
        {
          login: "vdloo",
          contributions: 8,
        },
        {
          login: "h8liu",
          contributions: 7,
        },
        {
          login: "cshung",
          contributions: 4,
        },
        {
          login: "filips123",
          contributions: 4,
        },
        {
          login: "BenNottelling",
          contributions: 4,
        },
        {
          login: "invernizzi",
          contributions: 3,
        },
        {
          login: "nwtgck",
          contributions: 3,
        },
        {
          login: "ShuoZheLi",
          contributions: 3,
        },
        {
          login: "viorelcanja",
          contributions: 3,
        },
        {
          login: "orklann",
          contributions: 2,
        },
        {
          login: "ikreymer",
          contributions: 2,
        },
        {
          login: "ysangkok",
          contributions: 2,
        },
        {
          login: "MarcinListkowski",
          contributions: 2,
        },
        {
          login: "skadisch",
          contributions: 2,
        },
        {
          login: "aidanhs",
          contributions: 1,
        },
        {
          login: "radarhere",
          contributions: 1,
        },
        {
          login: "subzey",
          contributions: 1,
        },
        {
          login: "Atybot",
          contributions: 1,
        },
        {
          login: "Hackerjef",
          contributions: 1,
        },
        {
          login: "cpressey",
          contributions: 1,
        },
      ],
    },
    {
      name: "actix-web",
      stars: 13060,
      stargazers_count: 13060,

      owner: {
        login: "actix",
      },
      repo: "actix-web",
      contributors: [
        {
          login: "fafhrd91",
          contributions: 2475,
        },
        {
          login: "robjtede",
          contributions: 438,
        },
        {
          login: "JohnTitor",
          contributions: 185,
        },
        {
          login: "fakeshadow",
          contributions: 123,
        },
        {
          login: "DoumanAsh",
          contributions: 57,
        },
        {
          login: "Dowwie",
          contributions: 38,
        },
        {
          login: "aliemjay",
          contributions: 29,
        },
        {
          login: "memoryruins",
          contributions: 26,
        },
        {
          login: "ibraheemdev",
          contributions: 25,
        },
        {
          login: "dunnock",
          contributions: 23,
        },
        {
          login: "Neopallium",
          contributions: 13,
        },
        {
          login: "messense",
          contributions: 12,
        },
        {
          login: "Aaron1011",
          contributions: 10,
        },
        {
          login: "mitsuhiko",
          contributions: 10,
        },
        {
          login: "ava57r",
          contributions: 9,
        },
        {
          login: "krircc",
          contributions: 8,
        },
        {
          login: "popzxc",
          contributions: 7,
        },
        {
          login: "mattgathu",
          contributions: 7,
        },
        {
          login: "fuchsnj",
          contributions: 7,
        },
        {
          login: "svenstaro",
          contributions: 7,
        },
        {
          login: "mockersf",
          contributions: 7,
        },
        {
          login: "adwhit",
          contributions: 6,
        },
        {
          login: "niklasf",
          contributions: 6,
        },
        {
          login: "radix",
          contributions: 5,
        },
        {
          login: "jrconlin",
          contributions: 5,
        },
      ],
    },
    {
      name: "iced",
      stars: 12674,
      stargazers_count: 12674,

      owner: {
        login: "iced-rs",
      },
      repo: "iced",
      contributors: [
        {
          login: "hecrj",
          contributions: 1788,
        },
        {
          login: "clarkmoody",
          contributions: 39,
        },
        {
          login: "tarkah",
          contributions: 18,
        },
        {
          login: "Kaiden42",
          contributions: 17,
        },
        {
          login: "Maldela",
          contributions: 17,
        },
        {
          login: "PolyMeilex",
          contributions: 16,
        },
        {
          login: "derezzedex",
          contributions: 16,
        },
        {
          login: "FabianLars",
          contributions: 14,
        },
        {
          login: "hatoo",
          contributions: 11,
        },
        {
          login: "Dispersia",
          contributions: 10,
        },
        {
          login: "Imberflur",
          contributions: 7,
        },
        {
          login: "daxpedda",
          contributions: 7,
        },
        {
          login: "BillyDM",
          contributions: 6,
        },
        {
          login: "Friz64",
          contributions: 6,
        },
        {
          login: "nicksenger",
          contributions: 6,
        },
        {
          login: "AlisCode",
          contributions: 6,
        },
        {
          login: "Songtronix",
          contributions: 5,
        },
        {
          login: "yusdacra",
          contributions: 5,
        },
        {
          login: "bansheerubber",
          contributions: 5,
        },
        {
          login: "13r0ck",
          contributions: 4,
        },
        {
          login: "Cupnfish",
          contributions: 4,
        },
        {
          login: "unrelentingtech",
          contributions: 4,
        },
        {
          login: "kaimast",
          contributions: 4,
        },
        {
          login: "Liamolucko",
          contributions: 4,
        },
        {
          login: "oknozor",
          contributions: 4,
        },
      ],
    },
    {
      name: "sonic",
      stars: 12518,
      stargazers_count: 12518,

      owner: {
        login: "valeriansaliou",
      },
      repo: "sonic",
      contributors: [
        {
          login: "valeriansaliou",
          contributions: 575,
        },
        {
          login: "pleshevskiy",
          contributions: 15,
        },
        {
          login: "perzanko",
          contributions: 4,
        },
        {
          login: "dsewnr",
          contributions: 4,
        },
        {
          login: "xmonader",
          contributions: 2,
        },
        {
          login: "BurtonQin",
          contributions: 2,
        },
        {
          login: "mikalv",
          contributions: 2,
        },
        {
          login: "vilunov",
          contributions: 2,
        },
        {
          login: "touhonoob",
          contributions: 2,
        },
        {
          login: "AlongWY",
          contributions: 1,
        },
        {
          login: "cederstrom",
          contributions: 1,
        },
        {
          login: "ltearno",
          contributions: 1,
        },
        {
          login: "batisteo",
          contributions: 1,
        },
        {
          login: "baptistejamin",
          contributions: 1,
        },
        {
          login: "imdario",
          contributions: 1,
        },
        {
          login: "dileepbapat",
          contributions: 1,
        },
        {
          login: "erfanium",
          contributions: 1,
        },
        {
          login: "hanspagel",
          contributions: 1,
        },
        {
          login: "imerkle",
          contributions: 1,
        },
        {
          login: "james2doyle",
          contributions: 1,
        },
        {
          login: "codeflows",
          contributions: 1,
        },
        {
          login: "OGKevin",
          contributions: 1,
        },
        {
          login: "mikeshatch",
          contributions: 1,
        },
        {
          login: "ppshobi",
          contributions: 1,
        },
        {
          login: "stewrightbauer",
          contributions: 1,
        },
      ],
    },
    {
      name: "cube.js",
      stars: 12254,
      stargazers_count: 12254,

      owner: {
        login: "cube-js",
      },
      repo: "cube.js",
      contributors: [
        {
          login: "paveltiunov",
          contributions: 2009,
        },
        {
          login: "ovr",
          contributions: 846,
        },
        {
          login: "vasilev-alex",
          contributions: 446,
        },
        {
          login: "keydunov",
          contributions: 372,
        },
        {
          login: "hassankhan",
          contributions: 352,
        },
        {
          login: "ilya-biryukov",
          contributions: 184,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 143,
        },
        {
          login: "dependabot[bot]",
          contributions: 85,
        },
        {
          login: "RusovDmitriy",
          contributions: 81,
        },
        {
          login: "igorlukanin",
          contributions: 60,
        },
        {
          login: "rpaik",
          contributions: 56,
        },
        {
          login: "YakovlevCoded",
          contributions: 53,
        },
        {
          login: "lvauvillier",
          contributions: 39,
        },
        {
          login: "adnanrahic",
          contributions: 37,
        },
        {
          login: "9teen90nine",
          contributions: 35,
        },
        {
          login: "rchkv",
          contributions: 27,
        },
        {
          login: "VictorTrapenok",
          contributions: 25,
        },
        {
          login: "tenphi",
          contributions: 22,
        },
        {
          login: "jcw-",
          contributions: 13,
        },
        {
          login: "willhausman",
          contributions: 12,
        },
        {
          login: "richipargo",
          contributions: 10,
        },
        {
          login: "dtslvr",
          contributions: 10,
        },
        {
          login: "rongfengliang",
          contributions: 10,
        },
        {
          login: "jkotova",
          contributions: 7,
        },
        {
          login: "hoota",
          contributions: 7,
        },
      ],
    },
    {
      name: "delta",
      stars: 12052,
      stargazers_count: 12052,

      owner: {
        login: "dandavison",
      },
      repo: "delta",
      contributors: [
        {
          login: "dandavison",
          contributions: 1516,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 59,
        },
        {
          login: "th1000s",
          contributions: 50,
        },
        {
          login: "dependabot[bot]",
          contributions: 24,
        },
        {
          login: "clnoll",
          contributions: 22,
        },
        {
          login: "MarcoIeni",
          contributions: 19,
        },
        {
          login: "m-lima",
          contributions: 7,
        },
        {
          login: "ulwlu",
          contributions: 7,
        },
        {
          login: "Kr1ss-XD",
          contributions: 7,
        },
        {
          login: "WayneD",
          contributions: 6,
        },
        {
          login: "navarroaxel",
          contributions: 4,
        },
        {
          login: "GuillaumeGomez",
          contributions: 3,
        },
        {
          login: "2KAbhishek",
          contributions: 2,
        },
        {
          login: "rouge8",
          contributions: 2,
        },
        {
          login: "fdav10",
          contributions: 2,
        },
        {
          login: "ignatenkobrain",
          contributions: 2,
        },
        {
          login: "lzybkr",
          contributions: 2,
        },
        {
          login: "maximd",
          contributions: 2,
        },
        {
          login: "pt2121",
          contributions: 2,
        },
        {
          login: "yoichi",
          contributions: 2,
        },
        {
          login: "ath3",
          contributions: 2,
        },
        {
          login: "est31",
          contributions: 2,
        },
        {
          login: "chtenb",
          contributions: 2,
        },
        {
          login: "hakamadare",
          contributions: 2,
        },
        {
          login: "anyu",
          contributions: 1,
        },
      ],
    },
    {
      name: "spotify-tui",
      stars: 11418,
      stargazers_count: 11418,

      owner: {
        login: "Rigellute",
      },
      repo: "spotify-tui",
      contributors: [
        {
          login: "Rigellute",
          contributions: 738,
        },
        {
          login: "allcontributors[bot]",
          contributions: 158,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 99,
        },
        {
          login: "jfaltis",
          contributions: 13,
        },
        {
          login: "sputnick1124",
          contributions: 11,
        },
        {
          login: "BKitor",
          contributions: 11,
        },
        {
          login: "fangyi-zhou",
          contributions: 10,
        },
        {
          login: "dependabot[bot]",
          contributions: 9,
        },
        {
          login: "Peterkmoss",
          contributions: 8,
        },
        {
          login: "OrangeFran",
          contributions: 7,
        },
        {
          login: "amgassert",
          contributions: 6,
        },
        {
          login: "LennyPenny",
          contributions: 6,
        },
        {
          login: "ilnaes",
          contributions: 6,
        },
        {
          login: "TimotheeGerber",
          contributions: 6,
        },
        {
          login: "hurricanehrndz",
          contributions: 5,
        },
        {
          login: "Utagai",
          contributions: 5,
        },
        {
          login: "pedrohva",
          contributions: 5,
        },
        {
          login: "TimonPost",
          contributions: 5,
        },
        {
          login: "zacklukem",
          contributions: 5,
        },
        {
          login: "fratajczak",
          contributions: 4,
        },
        {
          login: "gpawlik",
          contributions: 4,
        },
        {
          login: "Ricardo-HE",
          contributions: 4,
        },
        {
          login: "slumber",
          contributions: 4,
        },
        {
          login: "littleli",
          contributions: 3,
        },
        {
          login: "RadicalZephyr",
          contributions: 3,
        },
      ],
    },
    {
      name: "wasmer",
      stars: 11295,
      stargazers_count: 11295,

      owner: {
        login: "wasmerio",
      },
      repo: "wasmer",
      contributors: [
        {
          login: "syrusakbary",
          contributions: 3118,
        },
        {
          login: "Hywan",
          contributions: 1907,
        },
        {
          login: "MarkMcCaskey",
          contributions: 1571,
        },
        {
          login: "bors[bot]",
          contributions: 918,
        },
        {
          login: "nlewycky",
          contributions: 871,
        },
        {
          login: "losfair",
          contributions: 689,
        },
        {
          login: "bjfish",
          contributions: 430,
        },
        {
          login: "lachlansneff",
          contributions: 360,
        },
        {
          login: "xmclark",
          contributions: 214,
        },
        {
          login: "ptitSeb",
          contributions: 182,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 104,
        },
        {
          login: "appcypher",
          contributions: 96,
        },
        {
          login: "Amanieu",
          contributions: 77,
        },
        {
          login: "NathHorrigan",
          contributions: 66,
        },
        {
          login: "jubianchi",
          contributions: 66,
        },
        {
          login: "ailisp",
          contributions: 57,
        },
        {
          login: "pventuzelo",
          contributions: 52,
        },
        {
          login: "webmaster128",
          contributions: 49,
        },
        {
          login: "YaronWittenstein",
          contributions: 45,
        },
        {
          login: "chenyukang",
          contributions: 26,
        },
        {
          login: "jcaesar",
          contributions: 25,
        },
        {
          login: "natowi",
          contributions: 16,
        },
        {
          login: "piranna",
          contributions: 16,
        },
        {
          login: "Earthmark",
          contributions: 15,
        },
        {
          login: "bnjjj",
          contributions: 14,
        },
      ],
    },
    {
      name: "tikv",
      stars: 10615,
      stargazers_count: 10615,

      owner: {
        login: "tikv",
      },
      repo: "tikv",
      contributors: [
        {
          login: "BusyJay",
          contributions: 847,
        },
        {
          login: "siddontang",
          contributions: 658,
        },
        {
          login: "overvenus",
          contributions: 334,
        },
        {
          login: "ngaut",
          contributions: 308,
        },
        {
          login: "disksing",
          contributions: 282,
        },
        {
          login: "hicqu",
          contributions: 246,
        },
        {
          login: "breeswish",
          contributions: 231,
        },
        {
          login: "zhangjinpeng1987",
          contributions: 192,
        },
        {
          login: "AndreMouche",
          contributions: 149,
        },
        {
          login: "sticnarf",
          contributions: 138,
        },
        {
          login: "Connor1996",
          contributions: 109,
        },
        {
          login: "brson",
          contributions: 106,
        },
        {
          login: "5kbpers",
          contributions: 104,
        },
        {
          login: "huachaohuang",
          contributions: 98,
        },
        {
          login: "MyonKeminta",
          contributions: 98,
        },
        {
          login: "NingLin-P",
          contributions: 85,
        },
        {
          login: "morefreeze",
          contributions: 84,
        },
        {
          login: "youjiali1995",
          contributions: 83,
        },
        {
          login: "tabokie",
          contributions: 62,
        },
        {
          login: "lonng",
          contributions: 61,
        },
        {
          login: "yiwu-arbug",
          contributions: 59,
        },
        {
          login: "gengliqi",
          contributions: 57,
        },
        {
          login: "Little-Wallace",
          contributions: 56,
        },
        {
          login: "nrc",
          contributions: 55,
        },
        {
          login: "hhkbp2",
          contributions: 55,
        },
      ],
    },
    {
      name: "RustPython",
      stars: 10474,
      stargazers_count: 10474,

      owner: {
        login: "RustPython",
      },
      repo: "RustPython",
      contributors: [
        {
          login: "coolreader18",
          contributions: 2343,
        },
        {
          login: "youknowone",
          contributions: 1685,
        },
        {
          login: "windelbouwman",
          contributions: 1096,
        },
        {
          login: "palaviv",
          contributions: 723,
        },
        {
          login: "fanninpm",
          contributions: 668,
        },
        {
          login: "cthulahoops",
          contributions: 376,
        },
        {
          login: "DimitrisJim",
          contributions: 269,
        },
        {
          login: "OddCoincidence",
          contributions: 219,
        },
        {
          login: "skinny121",
          contributions: 188,
        },
        {
          login: "qingshi163",
          contributions: 170,
        },
        {
          login: "Snowapril",
          contributions: 119,
        },
        {
          login: "OddBloke",
          contributions: 109,
        },
        {
          login: "deantvv",
          contributions: 88,
        },
        {
          login: "rmliddle",
          contributions: 85,
        },
        {
          login: "ChJR",
          contributions: 78,
        },
        {
          login: "jgirardet",
          contributions: 69,
        },
        {
          login: "ZapAnton",
          contributions: 60,
        },
        {
          login: "moreal",
          contributions: 49,
        },
        {
          login: "HyeockJinKim",
          contributions: 46,
        },
        {
          login: "shinglyu",
          contributions: 46,
        },
        {
          login: "TheAnyKey",
          contributions: 46,
        },
        {
          login: "adrian17",
          contributions: 44,
        },
        {
          login: "carbotaniuman",
          contributions: 38,
        },
        {
          login: "Lynskylate",
          contributions: 37,
        },
        {
          login: "janczer",
          contributions: 34,
        },
      ],
    },
    {
      name: "navi",
      stars: 10220,
      stargazers_count: 10220,

      owner: {
        login: "denisidoro",
      },
      repo: "navi",
      contributors: [
        {
          login: "denisidoro",
          contributions: 483,
        },
        {
          login: "Kibouo",
          contributions: 34,
        },
        {
          login: "dependabot[bot]",
          contributions: 32,
        },
        {
          login: "mrVanDalo",
          contributions: 13,
        },
        {
          login: "DevAtDawn",
          contributions: 8,
        },
        {
          login: "kidonng",
          contributions: 8,
        },
        {
          login: "alxbl",
          contributions: 7,
        },
        {
          login: "bajatin",
          contributions: 6,
        },
        {
          login: "spoki0",
          contributions: 6,
        },
        {
          login: "aaronliu0130",
          contributions: 4,
        },
        {
          login: "djerfy",
          contributions: 4,
        },
        {
          login: "jmbataller",
          contributions: 3,
        },
        {
          login: "shenek",
          contributions: 3,
        },
        {
          login: "toan2406",
          contributions: 3,
        },
        {
          login: "wardwouts",
          contributions: 3,
        },
        {
          login: "b3s1r",
          contributions: 2,
        },
        {
          login: "enisozgen",
          contributions: 2,
        },
        {
          login: "kbknapp",
          contributions: 2,
        },
        {
          login: "pbsds",
          contributions: 2,
        },
        {
          login: "engrravijain",
          contributions: 2,
        },
        {
          login: "bitozoid",
          contributions: 2,
        },
        {
          login: "DenSA-Inc",
          contributions: 2,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 2,
        },
        {
          login: "PurpleBooth",
          contributions: 2,
        },
        {
          login: "amacfie",
          contributions: 1,
        },
      ],
    },
    {
      name: "hyperfine",
      stars: 10153,
      stargazers_count: 10153,

      owner: {
        login: "sharkdp",
      },
      repo: "hyperfine",
      contributors: [
        {
          login: "sharkdp",
          contributions: 260,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 96,
        },
        {
          login: "stevepentland",
          contributions: 33,
        },
        {
          login: "dependabot[bot]",
          contributions: 29,
        },
        {
          login: "piyushrungta25",
          contributions: 7,
        },
        {
          login: "jasonpeacock",
          contributions: 6,
        },
        {
          login: "scampi",
          contributions: 6,
        },
        {
          login: "knidarkness",
          contributions: 5,
        },
        {
          login: "wchargin",
          contributions: 4,
        },
        {
          login: "McMartin",
          contributions: 3,
        },
        {
          login: "AnderEnder",
          contributions: 3,
        },
        {
          login: "cadeef",
          contributions: 3,
        },
        {
          login: "chrisduerr",
          contributions: 3,
        },
        {
          login: "JordiChauzi",
          contributions: 3,
        },
        {
          login: "s1ck",
          contributions: 3,
        },
        {
          login: "iamsauravsharma",
          contributions: 3,
        },
        {
          login: "silathdiir",
          contributions: 3,
        },
        {
          login: "rhysd",
          contributions: 3,
        },
        {
          login: "b2m",
          contributions: 2,
        },
        {
          login: "colinwahl",
          contributions: 2,
        },
        {
          login: "orium",
          contributions: 2,
        },
        {
          login: "ghaiklor",
          contributions: 2,
        },
        {
          login: "JuanPotato",
          contributions: 2,
        },
        {
          login: "Calinou",
          contributions: 2,
        },
        {
          login: "remilauzier",
          contributions: 2,
        },
      ],
    },
    {
      name: "coreutils",
      stars: 9792,
      stargazers_count: 9792,

      owner: {
        login: "uutils",
      },
      repo: "coreutils",
      contributors: [
        {
          login: "sylvestre",
          contributions: 1023,
        },
        {
          login: "rivy",
          contributions: 645,
        },
        {
          login: "Arcterus",
          contributions: 639,
        },
        {
          login: "tertsdiepraam",
          contributions: 378,
        },
        {
          login: "ebfe",
          contributions: 340,
        },
        {
          login: "cnd",
          contributions: 331,
        },
        {
          login: "miDeb",
          contributions: 244,
        },
        {
          login: "jbcrail",
          contributions: 234,
        },
        {
          login: "jhscheer",
          contributions: 161,
        },
        {
          login: "jfinkels",
          contributions: 156,
        },
        {
          login: "nbraud",
          contributions: 132,
        },
        {
          login: "knight42",
          contributions: 95,
        },
        {
          login: "nathanross",
          contributions: 93,
        },
        {
          login: "hbina",
          contributions: 82,
        },
        {
          login: "backwaterred",
          contributions: 81,
        },
        {
          login: "kimono-koans",
          contributions: 77,
        },
        {
          login: "blyxxyz",
          contributions: 70,
        },
        {
          login: "Seldaek",
          contributions: 67,
        },
        {
          login: "jaggededgedjustice",
          contributions: 59,
        },
        {
          login: "thomasqueirozb",
          contributions: 47,
        },
        {
          login: "tilakpatidar",
          contributions: 46,
        },
        {
          login: "wimh",
          contributions: 44,
        },
        {
          login: "kwantam",
          contributions: 44,
        },
        {
          login: "youknowone",
          contributions: 41,
        },
        {
          login: "Funky185540",
          contributions: 39,
        },
      ],
    },
    {
      name: "static-analysis",
      stars: 9483,
      stargazers_count: 9483,

      owner: {
        login: "analysis-tools-dev",
      },
      repo: "static-analysis",
      contributors: [
        {
          login: "mre",
          contributions: 502,
        },
        {
          login: "AristoChen",
          contributions: 55,
        },
        {
          login: "dependabot[bot]",
          contributions: 39,
        },
        {
          login: "impredicative",
          contributions: 33,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 31,
        },
        {
          login: "andygrunwald",
          contributions: 17,
        },
        {
          login: "aliariff",
          contributions: 9,
        },
        {
          login: "brennanangel",
          contributions: 5,
        },
        {
          login: "jakubsacha",
          contributions: 5,
        },
        {
          login: "Mkohm",
          contributions: 5,
        },
        {
          login: "chadbrewbaker",
          contributions: 4,
        },
        {
          login: "ybiquitous",
          contributions: 4,
        },
        {
          login: "nvhaver",
          contributions: 4,
        },
        {
          login: "normanhh3",
          contributions: 4,
        },
        {
          login: "alexxn",
          contributions: 3,
        },
        {
          login: "arnecls",
          contributions: 3,
        },
        {
          login: "dspinellis",
          contributions: 3,
        },
        {
          login: "Glavin001",
          contributions: 3,
        },
        {
          login: "larshp",
          contributions: 3,
        },
        {
          login: "sobolevn",
          contributions: 3,
        },
        {
          login: "shaheemirza",
          contributions: 3,
        },
        {
          login: "chimurai",
          contributions: 3,
        },
        {
          login: "mschwager",
          contributions: 3,
        },
        {
          login: "u-combinator",
          contributions: 3,
        },
        {
          login: "saqibns",
          contributions: 3,
        },
      ],
    },
    {
      name: "pyxel",
      stars: 9362,
      stargazers_count: 9362,

      owner: {
        login: "kitao",
      },
      repo: "pyxel",
      contributors: [
        {
          login: "kitao",
          contributions: 3037,
        },
        {
          login: "dora-0",
          contributions: 12,
        },
        {
          login: "YifangSun",
          contributions: 11,
        },
        {
          login: "humrochagf",
          contributions: 9,
        },
        {
          login: "ccmorataya",
          contributions: 6,
        },
        {
          login: "DiddiLeija",
          contributions: 5,
        },
        {
          login: "JeanAraujo",
          contributions: 4,
        },
        {
          login: "gamwe6",
          contributions: 3,
        },
        {
          login: "merwok",
          contributions: 3,
        },
        {
          login: "grewn0uille",
          contributions: 3,
        },
        {
          login: "alxwrd",
          contributions: 2,
        },
        {
          login: "Lennart4711",
          contributions: 2,
        },
        {
          login: "roliveira",
          contributions: 2,
        },
        {
          login: "vesche",
          contributions: 2,
        },
        {
          login: "xyproto",
          contributions: 1,
        },
        {
          login: "arberx",
          contributions: 1,
        },
        {
          login: "tdh8316",
          contributions: 1,
        },
        {
          login: "Furqan17",
          contributions: 1,
        },
        {
          login: "fabiommendes",
          contributions: 1,
        },
        {
          login: "gediminasz",
          contributions: 1,
        },
        {
          login: "GiorgiBeriashvili",
          contributions: 1,
        },
        {
          login: "GitRenhl",
          contributions: 1,
        },
        {
          login: "HarryPeach",
          contributions: 1,
        },
        {
          login: "jmp",
          contributions: 1,
        },
        {
          login: "JoshuaKey",
          contributions: 1,
        },
      ],
    },
    {
      name: "ruffle",
      stars: 9319,
      stargazers_count: 9319,

      owner: {
        login: "ruffle-rs",
      },
      repo: "ruffle",
      contributors: [
        {
          login: "kmeisthax",
          contributions: 1752,
        },
        {
          login: "Herschel",
          contributions: 1432,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 757,
        },
        {
          login: "Dinnerbone",
          contributions: 566,
        },
        {
          login: "relrelb",
          contributions: 483,
        },
        {
          login: "dependabot[bot]",
          contributions: 191,
        },
        {
          login: "CUB3D",
          contributions: 163,
        },
        {
          login: "adrian17",
          contributions: 123,
        },
        {
          login: "EmperorBale",
          contributions: 115,
        },
        {
          login: "midgleyc",
          contributions: 70,
        },
        {
          login: "torokati44",
          contributions: 48,
        },
        {
          login: "Toad06",
          contributions: 47,
        },
        {
          login: "moulins",
          contributions: 45,
        },
        {
          login: "JustinCB",
          contributions: 44,
        },
        {
          login: "danielhjacobs",
          contributions: 20,
        },
        {
          login: "paq",
          contributions: 20,
        },
        {
          login: "kukininj",
          contributions: 19,
        },
        {
          login: "JMcKiern",
          contributions: 17,
        },
        {
          login: "madsmtm",
          contributions: 15,
        },
        {
          login: "naomiEve",
          contributions: 11,
        },
        {
          login: "Justin-CB",
          contributions: 10,
        },
        {
          login: "desuwa",
          contributions: 10,
        },
        {
          login: "hatal175",
          contributions: 9,
        },
        {
          login: "rdrpenguin04",
          contributions: 9,
        },
        {
          login: "Aaron1011",
          contributions: 8,
        },
      ],
    },
    {
      name: "hyper",
      stars: 9093,
      stargazers_count: 9093,

      owner: {
        login: "hyperium",
      },
      repo: "hyper",
      contributors: [
        {
          login: "seanmonstar",
          contributions: 1592,
        },
        {
          login: "reem",
          contributions: 134,
        },
        {
          login: "pyfisch",
          contributions: 63,
        },
        {
          login: "weihanglo",
          contributions: 25,
        },
        {
          login: "mlalic",
          contributions: 21,
        },
        {
          login: "sfackler",
          contributions: 19,
        },
        {
          login: "jwilm",
          contributions: 14,
        },
        {
          login: "nox",
          contributions: 13,
        },
        {
          login: "renato-zannon",
          contributions: 10,
        },
        {
          login: "s-panferov",
          contributions: 10,
        },
        {
          login: "taiki-e",
          contributions: 10,
        },
        {
          login: "jplatte",
          contributions: 10,
        },
        {
          login: "frewsxcv",
          contributions: 9,
        },
        {
          login: "yazaddaruvala",
          contributions: 8,
        },
        {
          login: "winding-lines",
          contributions: 7,
        },
        {
          login: "srijs",
          contributions: 7,
        },
        {
          login: "ubnt-intrepid",
          contributions: 7,
        },
        {
          login: "M3rs",
          contributions: 7,
        },
        {
          login: "mikedilger",
          contributions: 7,
        },
        {
          login: "hugoduncan",
          contributions: 6,
        },
        {
          login: "jgillich",
          contributions: 6,
        },
        {
          login: "Byron",
          contributions: 6,
        },
        {
          login: "lnicola",
          contributions: 6,
        },
        {
          login: "cyderize",
          contributions: 6,
        },
        {
          login: "hawkw",
          contributions: 5,
        },
      ],
    },
    {
      name: "vector",
      stars: 9016,
      stargazers_count: 9016,

      owner: {
        login: "vectordotdev",
      },
      repo: "vector",
      contributors: [
        {
          login: "binarylogic",
          contributions: 1235,
        },
        {
          login: "jszwedko",
          contributions: 612,
        },
        {
          login: "dependabot[bot]",
          contributions: 591,
        },
        {
          login: "lukesteensen",
          contributions: 332,
        },
        {
          login: "bruceg",
          contributions: 330,
        },
        {
          login: "LucioFranco",
          contributions: 257,
        },
        {
          login: "ktff",
          contributions: 235,
        },
        {
          login: "a-rodin",
          contributions: 209,
        },
        {
          login: "fanatid",
          contributions: 195,
        },
        {
          login: "lucperkins",
          contributions: 181,
        },
        {
          login: "michaelfairley",
          contributions: 168,
        },
        {
          login: "jamtur01",
          contributions: 165,
        },
        {
          login: "blt",
          contributions: 150,
        },
        {
          login: "MOZGIII",
          contributions: 148,
        },
        {
          login: "Hoverbear",
          contributions: 124,
        },
        {
          login: "StephenWakely",
          contributions: 118,
        },
        {
          login: "JeanMertz",
          contributions: 116,
        },
        {
          login: "spencergilbert",
          contributions: 96,
        },
        {
          login: "leebenson",
          contributions: 93,
        },
        {
          login: "jdrouet",
          contributions: 83,
        },
        {
          login: "Jeffail",
          contributions: 66,
        },
        {
          login: "pablosichert",
          contributions: 63,
        },
        {
          login: "tobz",
          contributions: 60,
        },
        {
          login: "juchiast",
          contributions: 58,
        },
        {
          login: "001wwang",
          contributions: 47,
        },
      ],
    },
    {
      name: "xray",
      stars: 8565,
      stargazers_count: 8565,

      owner: {
        login: "atom-archive",
      },
      repo: "xray",
      contributors: [
        {
          login: "maxbrunsfeld",
          contributions: 43,
        },
        {
          login: "joshaber",
          contributions: 26,
        },
        {
          login: "jasonrudolph",
          contributions: 24,
        },
        {
          login: "dirk",
          contributions: 23,
        },
        {
          login: "chgibb",
          contributions: 13,
        },
        {
          login: "max-sixty",
          contributions: 4,
        },
        {
          login: "eyelash",
          contributions: 4,
        },
        {
          login: "matthewwithanm",
          contributions: 3,
        },
        {
          login: "MoritzKn",
          contributions: 3,
        },
        {
          login: "felixrabe",
          contributions: 2,
        },
        {
          login: "arun-is",
          contributions: 1,
        },
        {
          login: "Aerijo",
          contributions: 1,
        },
        {
          login: "DesWurstes",
          contributions: 1,
        },
        {
          login: "Jpunt",
          contributions: 1,
        },
        {
          login: "leroix",
          contributions: 1,
        },
        {
          login: "karlbecker",
          contributions: 1,
        },
        {
          login: "Arcanemagus",
          contributions: 1,
        },
        {
          login: "Nitrino",
          contributions: 1,
        },
        {
          login: "haacked",
          contributions: 1,
        },
        {
          login: "b-fuze",
          contributions: 1,
        },
        {
          login: "janczer",
          contributions: 1,
        },
        {
          login: "rleungx",
          contributions: 1,
        },
        {
          login: "yajamon",
          contributions: 1,
        },
      ],
    },
    {
      name: "book",
      stars: 8560,
      stargazers_count: 8560,

      owner: {
        login: "rust-lang",
      },
      repo: "book",
      contributors: [
        {
          login: "carols10cents",
          contributions: 2791,
        },
        {
          login: "steveklabnik",
          contributions: 624,
        },
        {
          login: "davidde",
          contributions: 29,
        },
        {
          login: "gypsydave5",
          contributions: 18,
        },
        {
          login: "u32i64",
          contributions: 15,
        },
        {
          login: "chenl",
          contributions: 15,
        },
        {
          login: "pduzinki",
          contributions: 12,
        },
        {
          login: "JIghtuse",
          contributions: 11,
        },
        {
          login: "bmusin",
          contributions: 11,
        },
        {
          login: "aaaxx",
          contributions: 9,
        },
        {
          login: "lancelafontaine",
          contributions: 9,
        },
        {
          login: "elahn",
          contributions: 8,
        },
        {
          login: "ehuss",
          contributions: 8,
        },
        {
          login: "outkaj",
          contributions: 8,
        },
        {
          login: "spastorino",
          contributions: 8,
        },
        {
          login: "appleJax",
          contributions: 8,
        },
        {
          login: "clemensw",
          contributions: 7,
        },
        {
          login: "frewsxcv",
          contributions: 7,
        },
        {
          login: "sebras",
          contributions: 7,
        },
        {
          login: "JosephTLyons",
          contributions: 7,
        },
        {
          login: "UnHumbleBen",
          contributions: 6,
        },
        {
          login: "dralley",
          contributions: 6,
        },
        {
          login: "mimoo",
          contributions: 6,
        },
        {
          login: "GrantMoyer",
          contributions: 6,
        },
        {
          login: "GuillaumeGomez",
          contributions: 6,
        },
      ],
    },
    {
      name: "mdBook",
      stars: 8489,
      stargazers_count: 8489,

      owner: {
        login: "rust-lang",
      },
      repo: "mdBook",
      contributors: [
        {
          login: "azerupi",
          contributions: 427,
        },
        {
          login: "ehuss",
          contributions: 321,
        },
        {
          login: "Michael-F-Bryan",
          contributions: 213,
        },
        {
          login: "mattico",
          contributions: 103,
        },
        {
          login: "budziq",
          contributions: 56,
        },
        {
          login: "steveklabnik",
          contributions: 50,
        },
        {
          login: "joshrotenberg",
          contributions: 44,
        },
        {
          login: "Dylan-DPC",
          contributions: 32,
        },
        {
          login: "carols10cents",
          contributions: 22,
        },
        {
          login: "sorin-davidoi",
          contributions: 20,
        },
        {
          login: "tesuji",
          contributions: 20,
        },
        {
          login: "JaimeValdemoros",
          contributions: 18,
        },
        {
          login: "frewsxcv",
          contributions: 15,
        },
        {
          login: "mbrubeck",
          contributions: 13,
        },
        {
          login: "gambhiro",
          contributions: 11,
        },
        {
          login: "mdinger",
          contributions: 11,
        },
        {
          login: "sunng87",
          contributions: 10,
        },
        {
          login: "Bobo1239",
          contributions: 8,
        },
        {
          login: "camelid",
          contributions: 8,
        },
        {
          login: "behnam",
          contributions: 7,
        },
        {
          login: "Bassetts",
          contributions: 7,
        },
        {
          login: "apatniv",
          contributions: 7,
        },
        {
          login: "benediktwerner",
          contributions: 6,
        },
        {
          login: "bmusin",
          contributions: 6,
        },
        {
          login: "dtolnay",
          contributions: 6,
        },
      ],
    },
    {
      name: "rust-analyzer",
      stars: 8360,
      stargazers_count: 8360,

      owner: {
        login: "rust-analyzer",
      },
      repo: "rust-analyzer",
      contributors: [
        {
          login: "bors[bot]",
          contributions: 6409,
        },
        {
          login: "matklad",
          contributions: 5283,
        },
        {
          login: "Veykril",
          contributions: 1037,
        },
        {
          login: "jonas-schievink",
          contributions: 772,
        },
        {
          login: "flodiebold",
          contributions: 763,
        },
        {
          login: "kjeremy",
          contributions: 571,
        },
        {
          login: "edwin0cheng",
          contributions: 528,
        },
        {
          login: "lnicola",
          contributions: 463,
        },
        {
          login: "SomeoneToIgnore",
          contributions: 387,
        },
        {
          login: "Veetaha",
          contributions: 182,
        },
        {
          login: "vsrs",
          contributions: 130,
        },
        {
          login: "DJMcNab",
          contributions: 125,
        },
        {
          login: "bnjjj",
          contributions: 115,
        },
        {
          login: "vipentti",
          contributions: 99,
        },
        {
          login: "iDawer",
          contributions: 96,
        },
        {
          login: "popzxc",
          contributions: 85,
        },
        {
          login: "Nashenas88",
          contributions: 80,
        },
        {
          login: "kiljacken",
          contributions: 74,
        },
        {
          login: "oxalica",
          contributions: 74,
        },
        {
          login: "yoshuawuyts",
          contributions: 71,
        },
        {
          login: "jhgg",
          contributions: 64,
        },
        {
          login: "JoshMcguigan",
          contributions: 62,
        },
        {
          login: "vemoo",
          contributions: 61,
        },
        {
          login: "zacps",
          contributions: 53,
        },
        {
          login: "seivan",
          contributions: 52,
        },
      ],
    },
    {
      name: "zola",
      stars: 8047,
      stargazers_count: 8047,

      owner: {
        login: "getzola",
      },
      repo: "zola",
      contributors: [
        {
          login: "Keats",
          contributions: 591,
        },
        {
          login: "codesections",
          contributions: 16,
        },
        {
          login: "rootkea",
          contributions: 15,
        },
        {
          login: "samford",
          contributions: 14,
        },
        {
          login: "chris-morgan",
          contributions: 12,
        },
        {
          login: "vojtechkral",
          contributions: 12,
        },
        {
          login: "southerntofu",
          contributions: 12,
        },
        {
          login: "Freaky",
          contributions: 11,
        },
        {
          login: "paulcmal",
          contributions: 11,
        },
        {
          login: "savente93",
          contributions: 8,
        },
        {
          login: "peng1999",
          contributions: 8,
        },
        {
          login: "uggla",
          contributions: 7,
        },
        {
          login: "bdjnk",
          contributions: 7,
        },
        {
          login: "ErichDonGubler",
          contributions: 6,
        },
        {
          login: "homeworkprod",
          contributions: 6,
        },
        {
          login: "onelson",
          contributions: 6,
        },
        {
          login: "Libbum",
          contributions: 5,
        },
        {
          login: "mre",
          contributions: 5,
        },
        {
          login: "actions-user",
          contributions: 4,
        },
        {
          login: "dancek",
          contributions: 4,
        },
        {
          login: "jamesmunns",
          contributions: 4,
        },
        {
          login: "PhilipDaniels",
          contributions: 4,
        },
        {
          login: "reillysiemens",
          contributions: 4,
        },
        {
          login: "thomasetter",
          contributions: 4,
        },
        {
          login: "williamyaoh",
          contributions: 4,
        },
      ],
    },
    {
      name: "xsv",
      stars: 7923,
      stargazers_count: 7923,

      owner: {
        login: "BurntSushi",
      },
      repo: "xsv",
      contributors: [
        {
          login: "BurntSushi",
          contributions: 369,
        },
        {
          login: "tafia",
          contributions: 4,
        },
        {
          login: "kper",
          contributions: 3,
        },
        {
          login: "Dimagog",
          contributions: 2,
        },
        {
          login: "emk",
          contributions: 2,
        },
        {
          login: "Yomguithereal",
          contributions: 2,
        },
        {
          login: "alexcrichton",
          contributions: 1,
        },
        {
          login: "atouchet",
          contributions: 1,
        },
        {
          login: "AlexArgoAi",
          contributions: 1,
        },
        {
          login: "amilajack",
          contributions: 1,
        },
        {
          login: "astro",
          contributions: 1,
        },
        {
          login: "bruceadams",
          contributions: 1,
        },
        {
          login: "cbuesser",
          contributions: 1,
        },
        {
          login: "chills42",
          contributions: 1,
        },
        {
          login: "mdamien",
          contributions: 1,
        },
        {
          login: "josephfrazier",
          contributions: 1,
        },
        {
          login: "kbd",
          contributions: 1,
        },
        {
          login: "kyegupov",
          contributions: 1,
        },
        {
          login: "ldcasillas-progreso",
          contributions: 1,
        },
        {
          login: "LukasKalbertodt",
          contributions: 1,
        },
        {
          login: "mehandes",
          contributions: 1,
        },
        {
          login: "mchesser",
          contributions: 1,
        },
        {
          login: "Migi",
          contributions: 1,
        },
        {
          login: "lperry",
          contributions: 1,
        },
        {
          login: "Timmmm",
          contributions: 1,
        },
      ],
    },
    {
      name: "diesel",
      stars: 7898,
      stargazers_count: 7898,

      owner: {
        login: "diesel-rs",
      },
      repo: "diesel",
      contributors: [
        {
          login: "sgrif",
          contributions: 1865,
        },
        {
          login: "weiznich",
          contributions: 994,
        },
        {
          login: "killercup",
          contributions: 189,
        },
        {
          login: "Eijebong",
          contributions: 130,
        },
        {
          login: "TaKO8Ki",
          contributions: 70,
        },
        {
          login: "Ten0",
          contributions: 58,
        },
        {
          login: "h-michael",
          contributions: 46,
        },
        {
          login: "mcasper",
          contributions: 45,
        },
        {
          login: "JohnTitor",
          contributions: 38,
        },
        {
          login: "hi-rustin",
          contributions: 32,
        },
        {
          login: "KeenS",
          contributions: 30,
        },
        {
          login: "theredfish",
          contributions: 25,
        },
        {
          login: "mfpiccolo",
          contributions: 23,
        },
        {
          login: "pickfire",
          contributions: 22,
        },
        {
          login: "pksunkara",
          contributions: 22,
        },
        {
          login: "thekuom",
          contributions: 22,
        },
        {
          login: "Razican",
          contributions: 22,
        },
        {
          login: "notryanb",
          contributions: 20,
        },
        {
          login: "dbrgn",
          contributions: 16,
        },
        {
          login: "Emm",
          contributions: 16,
        },
        {
          login: "pfernie",
          contributions: 16,
        },
        {
          login: "oeed",
          contributions: 12,
        },
        {
          login: "ropottnik",
          contributions: 12,
        },
        {
          login: "asafigan",
          contributions: 11,
        },
        {
          login: "gobanos",
          contributions: 11,
        },
      ],
    },
    {
      name: "py-spy",
      stars: 7874,
      stargazers_count: 7874,

      owner: {
        login: "benfred",
      },
      repo: "py-spy",
      contributors: [
        {
          login: "benfred",
          contributions: 349,
        },
        {
          login: "dependabot[bot]",
          contributions: 25,
        },
        {
          login: "github-actions[bot]",
          contributions: 11,
        },
        {
          login: "Jongy",
          contributions: 9,
        },
        {
          login: "akhramov",
          contributions: 5,
        },
        {
          login: "messense",
          contributions: 5,
        },
        {
          login: "jarus",
          contributions: 4,
        },
        {
          login: "adamchainz",
          contributions: 3,
        },
        {
          login: "marcstreeter",
          contributions: 3,
        },
        {
          login: "kxepal",
          contributions: 2,
        },
        {
          login: "achanda",
          contributions: 1,
        },
        {
          login: "anntzer",
          contributions: 1,
        },
        {
          login: "IceTDrinker",
          contributions: 1,
        },
        {
          login: "ckw017",
          contributions: 1,
        },
        {
          login: "cclauss",
          contributions: 1,
        },
        {
          login: "craftyguy",
          contributions: 1,
        },
        {
          login: "auscompgeek",
          contributions: 1,
        },
        {
          login: "domartynov",
          contributions: 1,
        },
        {
          login: "frederikaalund",
          contributions: 1,
        },
        {
          login: "gjoseph92",
          contributions: 1,
        },
        {
          login: "liZe",
          contributions: 1,
        },
        {
          login: "jugmac00",
          contributions: 1,
        },
        {
          login: "Nudin",
          contributions: 1,
        },
        {
          login: "mindriot101",
          contributions: 1,
        },
        {
          login: "ThomasWaldmann",
          contributions: 1,
        },
      ],
    },
    {
      name: "cargo",
      stars: 7766,
      stargazers_count: 7766,

      owner: {
        login: "rust-lang",
      },
      repo: "cargo",
      contributors: [
        {
          login: "bors",
          contributions: 3887,
        },
        {
          login: "alexcrichton",
          contributions: 1506,
        },
        {
          login: "ehuss",
          contributions: 1099,
        },
        {
          login: "Eh2406",
          contributions: 385,
        },
        {
          login: "matklad",
          contributions: 368,
        },
        {
          login: "dwijnand",
          contributions: 323,
        },
        {
          login: "weihanglo",
          contributions: 151,
        },
        {
          login: "wycats",
          contributions: 104,
        },
        {
          login: "carols10cents",
          contributions: 77,
        },
        {
          login: "carllerche",
          contributions: 73,
        },
        {
          login: "djc",
          contributions: 73,
        },
        {
          login: "jonhoo",
          contributions: 72,
        },
        {
          login: "da-x",
          contributions: 70,
        },
        {
          login: "hi-rustin",
          contributions: 58,
        },
        {
          login: "behnam",
          contributions: 52,
        },
        {
          login: "volks73",
          contributions: 50,
        },
        {
          login: "steveklabnik",
          contributions: 50,
        },
        {
          login: "matthiaskrgr",
          contributions: 48,
        },
        {
          login: "Aaron1011",
          contributions: 47,
        },
        {
          login: "yaahc",
          contributions: 41,
        },
        {
          login: "brson",
          contributions: 41,
        },
        {
          login: "Mark-Simulacrum",
          contributions: 38,
        },
        {
          login: "tilde-engineering",
          contributions: 37,
        },
        {
          login: "willcrichton",
          contributions: 36,
        },
        {
          login: "nipunn1313",
          contributions: 36,
        },
      ],
    },
    {
      name: "amethyst",
      stars: 7740,
      stargazers_count: 7740,

      owner: {
        login: "amethyst",
      },
      repo: "amethyst",
      contributors: [
        {
          login: "bors[bot]",
          contributions: 864,
        },
        {
          login: "jojolepro",
          contributions: 507,
        },
        {
          login: "ebkalderon",
          contributions: 492,
        },
        {
          login: "azriel91",
          contributions: 419,
        },
        {
          login: "ezpuzz",
          contributions: 412,
        },
        {
          login: "Xaeroxe",
          contributions: 314,
        },
        {
          login: "torkleyy",
          contributions: 265,
        },
        {
          login: "Frizi",
          contributions: 187,
        },
        {
          login: "Rhuagh",
          contributions: 170,
        },
        {
          login: "jaynus",
          contributions: 164,
        },
        {
          login: "CleanCut",
          contributions: 151,
        },
        {
          login: "nchashch",
          contributions: 139,
        },
        {
          login: "AlveLarsson",
          contributions: 132,
        },
        {
          login: "Aceeri",
          contributions: 112,
        },
        {
          login: "mrhatman",
          contributions: 105,
        },
        {
          login: "grzi",
          contributions: 78,
        },
        {
          login: "valkum",
          contributions: 62,
        },
        {
          login: "jlowry",
          contributions: 59,
        },
        {
          login: "LucioFranco",
          contributions: 49,
        },
        {
          login: "amethyst-bors",
          contributions: 47,
        },
        {
          login: "udoprog",
          contributions: 47,
        },
        {
          login: "fhaynes",
          contributions: 44,
        },
        {
          login: "chemicstry",
          contributions: 44,
        },
        {
          login: "Moxinilian",
          contributions: 40,
        },
        {
          login: "csherratt",
          contributions: 39,
        },
      ],
    },
    {
      name: "clap",
      stars: 7707,
      stargazers_count: 7707,

      owner: {
        login: "clap-rs",
      },
      repo: "clap",
      contributors: [
        {
          login: "kbknapp",
          contributions: 1745,
        },
        {
          login: "epage",
          contributions: 605,
        },
        {
          login: "pksunkara",
          contributions: 403,
        },
        {
          login: "bors[bot]",
          contributions: 327,
        },
        {
          login: "homu",
          contributions: 253,
        },
        {
          login: "CreepySkeleton",
          contributions: 145,
        },
        {
          login: "ldm0",
          contributions: 130,
        },
        {
          login: "Vinatorul",
          contributions: 89,
        },
        {
          login: "TeXitoi",
          contributions: 89,
        },
        {
          login: "willmurphyscode",
          contributions: 33,
        },
        {
          login: "little-dude",
          contributions: 31,
        },
        {
          login: "NickHackman",
          contributions: 31,
        },
        {
          login: "williamyaoh",
          contributions: 29,
        },
        {
          login: "tormol",
          contributions: 28,
        },
        {
          login: "Dylan-DPC",
          contributions: 24,
        },
        {
          login: "rami3l",
          contributions: 23,
        },
        {
          login: "sru",
          contributions: 22,
        },
        {
          login: "gingerDevilish",
          contributions: 21,
        },
        {
          login: "mgeisler",
          contributions: 20,
        },
        {
          login: "mkantor",
          contributions: 17,
        },
        {
          login: "nabijaczleweli",
          contributions: 17,
        },
        {
          login: "fishface60",
          contributions: 16,
        },
        {
          login: "ModProg",
          contributions: 16,
        },
        {
          login: "Byron",
          contributions: 15,
        },
        {
          login: "cstyles",
          contributions: 12,
        },
      ],
    },
    {
      name: "rust-raspberrypi-OS-tutorials",
      stars: 7547,
      stargazers_count: 7547,

      owner: {
        login: "rust-embedded",
      },
      repo: "rust-raspberrypi-OS-tutorials",
      contributors: [
        {
          login: "andre-richter",
          contributions: 574,
        },
        {
          login: "rahealy",
          contributions: 3,
        },
        {
          login: "colachg",
          contributions: 3,
        },
        {
          login: "readlnh",
          contributions: 3,
        },
        {
          login: "berkus",
          contributions: 2,
        },
        {
          login: "naotaco",
          contributions: 2,
        },
        {
          login: "am009",
          contributions: 2,
        },
        {
          login: "jjyr",
          contributions: 2,
        },
        {
          login: "abbec",
          contributions: 1,
        },
        {
          login: "ZhangHanDong",
          contributions: 1,
        },
        {
          login: "cfsamson",
          contributions: 1,
        },
        {
          login: "DouglasUrner",
          contributions: 1,
        },
        {
          login: "IfanTsai",
          contributions: 1,
        },
        {
          login: "wizofe",
          contributions: 1,
        },
        {
          login: "pfriesch",
          contributions: 1,
        },
        {
          login: "tjhu",
          contributions: 1,
        },
        {
          login: "tylerchr",
          contributions: 1,
        },
        {
          login: "heaven0sky",
          contributions: 1,
        },
        {
          login: "jetjinser",
          contributions: 1,
        },
        {
          login: "light4",
          contributions: 1,
        },
        {
          login: "myl7",
          contributions: 1,
        },
      ],
    },
    {
      name: "tree-sitter",
      stars: 7442,
      stargazers_count: 7442,

      owner: {
        login: "tree-sitter",
      },
      repo: "tree-sitter",
      contributors: [
        {
          login: "maxbrunsfeld",
          contributions: 3057,
        },
        {
          login: "patrickt",
          contributions: 88,
        },
        {
          login: "ahlinc",
          contributions: 71,
        },
        {
          login: "joshvera",
          contributions: 48,
        },
        {
          login: "philipturnbull",
          contributions: 44,
        },
        {
          login: "tclem",
          contributions: 38,
        },
        {
          login: "dcreager",
          contributions: 27,
        },
        {
          login: "ahelwer",
          contributions: 21,
        },
        {
          login: "robrix",
          contributions: 19,
        },
        {
          login: "hendrikvanantwerpen",
          contributions: 16,
        },
        {
          login: "ikatyang",
          contributions: 10,
        },
        {
          login: "ubolonton",
          contributions: 9,
        },
        {
          login: "wingrunr21",
          contributions: 8,
        },
        {
          login: "nhasabni",
          contributions: 7,
        },
        {
          login: "bfredl",
          contributions: 6,
        },
        {
          login: "mkvoya",
          contributions: 6,
        },
        {
          login: "the-mikedavis",
          contributions: 5,
        },
        {
          login: "CyberShadow",
          contributions: 5,
        },
        {
          login: "ikrima",
          contributions: 5,
        },
        {
          login: "SKalt",
          contributions: 5,
        },
        {
          login: "razzeee",
          contributions: 5,
        },
        {
          login: "kramred",
          contributions: 4,
        },
        {
          login: "mkrupcale",
          contributions: 4,
        },
        {
          login: "narpfel",
          contributions: 4,
        },
        {
          login: "stsewd",
          contributions: 4,
        },
      ],
    },
    {
      name: "gitui",
      stars: 7164,
      stargazers_count: 7164,

      owner: {
        login: "extrawurst",
      },
      repo: "gitui",
      contributors: [
        {
          login: "extrawurst",
          contributions: 1318,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 100,
        },
        {
          login: "dependabot[bot]",
          contributions: 32,
        },
        {
          login: "WizardOhio24",
          contributions: 23,
        },
        {
          login: "cruessler",
          contributions: 16,
        },
        {
          login: "ImgBotApp",
          contributions: 14,
        },
        {
          login: "jonstodle",
          contributions: 7,
        },
        {
          login: "yanganto",
          contributions: 6,
        },
        {
          login: "MCord",
          contributions: 6,
        },
        {
          login: "alocquet",
          contributions: 6,
        },
        {
          login: "pm100",
          contributions: 5,
        },
        {
          login: "alistaircarscadden",
          contributions: 5,
        },
        {
          login: "remique",
          contributions: 4,
        },
        {
          login: "alessandroasm",
          contributions: 3,
        },
        {
          login: "Cottser",
          contributions: 3,
        },
        {
          login: "tisorlawan",
          contributions: 2,
        },
        {
          login: "silmeth",
          contributions: 2,
        },
        {
          login: "vladimyr",
          contributions: 2,
        },
        {
          login: "ignatenkobrain",
          contributions: 2,
        },
        {
          login: "olocoder",
          contributions: 2,
        },
        {
          login: "R0nd",
          contributions: 2,
        },
        {
          login: "wandernauta",
          contributions: 2,
        },
        {
          login: "andrewpollack",
          contributions: 2,
        },
        {
          login: "dm9pZCAq",
          contributions: 2,
        },
        {
          login: "zcorniere",
          contributions: 2,
        },
      ],
    },
    {
      name: "talent-plan",
      stars: 7077,
      stargazers_count: 7077,

      owner: {
        login: "pingcap",
      },
      repo: "talent-plan",
      contributors: [
        {
          login: "brson",
          contributions: 203,
        },
        {
          login: "sticnarf",
          contributions: 92,
        },
        {
          login: "overvenus",
          contributions: 65,
        },
        {
          login: "rleungx",
          contributions: 11,
        },
        {
          login: "maxcountryman",
          contributions: 8,
        },
        {
          login: "nolouch",
          contributions: 8,
        },
        {
          login: "Soline324",
          contributions: 8,
        },
        {
          login: "hi-rustin",
          contributions: 8,
        },
        {
          login: "Hoverbear",
          contributions: 6,
        },
        {
          login: "lamxTyler",
          contributions: 6,
        },
        {
          login: "ngaut",
          contributions: 6,
        },
        {
          login: "HunDunDM",
          contributions: 6,
        },
        {
          login: "Connor1996",
          contributions: 5,
        },
        {
          login: "mxinden",
          contributions: 5,
        },
        {
          login: "b41sh",
          contributions: 5,
        },
        {
          login: "yanguwan",
          contributions: 5,
        },
        {
          login: "gj",
          contributions: 4,
        },
        {
          login: "zz-jason",
          contributions: 4,
        },
        {
          login: "qw4990",
          contributions: 4,
        },
        {
          login: "aknuds1",
          contributions: 3,
        },
        {
          login: "benpigchu",
          contributions: 3,
        },
        {
          login: "connec",
          contributions: 3,
        },
        {
          login: "FuShixiong",
          contributions: 3,
        },
        {
          login: "printfinn",
          contributions: 3,
        },
        {
          login: "loxp",
          contributions: 3,
        },
      ],
    },
    {
      name: "solana",
      stars: 7072,
      stargazers_count: 7072,

      owner: {
        login: "solana-labs",
      },
      repo: "solana",
      contributors: [
        {
          login: "mvines",
          contributions: 3865,
        },
        {
          login: "garious",
          contributions: 1797,
        },
        {
          login: "dependabot[bot]",
          contributions: 1596,
        },
        {
          login: "CriesofCarrots",
          contributions: 1012,
        },
        {
          login: "jackcmay",
          contributions: 821,
        },
        {
          login: "jstarry",
          contributions: 779,
        },
        {
          login: "sakridge",
          contributions: 723,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 687,
        },
        {
          login: "rob-solana",
          contributions: 658,
        },
        {
          login: "jeffwashington",
          contributions: 637,
        },
        {
          login: "t-nelson",
          contributions: 569,
        },
        {
          login: "carllin",
          contributions: 505,
        },
        {
          login: "pgarg66",
          contributions: 432,
        },
        {
          login: "aeyakovenko",
          contributions: 426,
        },
        {
          login: "danpaul000",
          contributions: 272,
        },
        {
          login: "ryoqun",
          contributions: 271,
        },
        {
          login: "behzadnouri",
          contributions: 254,
        },
        {
          login: "sagar-solana",
          contributions: 224,
        },
        {
          login: "oJshua",
          contributions: 146,
        },
        {
          login: "brooksprumo",
          contributions: 136,
        },
        {
          login: "joeaba",
          contributions: 101,
        },
        {
          login: "samkim-crypto",
          contributions: 84,
        },
        {
          login: "Lichtso",
          contributions: 78,
        },
        {
          login: "joncinque",
          contributions: 56,
        },
        {
          login: "dmakarov",
          contributions: 56,
        },
      ],
    },
    {
      name: "rust-clippy",
      stars: 6949,
      stargazers_count: 6949,

      owner: {
        login: "rust-lang",
      },
      repo: "rust-clippy",
      contributors: [
        {
          login: "bors",
          contributions: 2349,
        },
        {
          login: "oli-obk",
          contributions: 1129,
        },
        {
          login: "flip1995",
          contributions: 913,
        },
        {
          login: "Manishearth",
          contributions: 806,
        },
        {
          login: "mcarton",
          contributions: 654,
        },
        {
          login: "phansch",
          contributions: 608,
        },
        {
          login: "llogiq",
          contributions: 537,
        },
        {
          login: "matthiaskrgr",
          contributions: 438,
        },
        {
          login: "mikerite",
          contributions: 318,
        },
        {
          login: "camsteffen",
          contributions: 261,
        },
        {
          login: "JohnTitor",
          contributions: 157,
        },
        {
          login: "tesuji",
          contributions: 155,
        },
        {
          login: "ebroto",
          contributions: 146,
        },
        {
          login: "ThibsG",
          contributions: 142,
        },
        {
          login: "Jarcho",
          contributions: 129,
        },
        {
          login: "xFrednet",
          contributions: 129,
        },
        {
          login: "birkenfeld",
          contributions: 124,
        },
        {
          login: "giraffate",
          contributions: 102,
        },
        {
          login: "sinkuu",
          contributions: 99,
        },
        {
          login: "Y-Nak",
          contributions: 90,
        },
        {
          login: "TaKO8Ki",
          contributions: 85,
        },
        {
          login: "rail-rain",
          contributions: 77,
        },
        {
          login: "devonhollowood",
          contributions: 70,
        },
        {
          login: "luisbg",
          contributions: 58,
        },
        {
          login: "krishna-veerareddy",
          contributions: 56,
        },
      ],
    },
    {
      name: "lsd",
      stars: 6928,
      stargazers_count: 6928,

      owner: {
        login: "Peltoche",
      },
      repo: "lsd",
      contributors: [
        {
          login: "Peltoche",
          contributions: 239,
        },
        {
          login: "zwpaper",
          contributions: 76,
        },
        {
          login: "meain",
          contributions: 75,
        },
        {
          login: "0jdxt",
          contributions: 41,
        },
        {
          login: "dvvvvvv",
          contributions: 32,
        },
        {
          login: "rivy",
          contributions: 25,
        },
        {
          login: "999eagle",
          contributions: 18,
        },
        {
          login: "danieldulaney",
          contributions: 14,
        },
        {
          login: "OrangeFran",
          contributions: 14,
        },
        {
          login: "hemreari",
          contributions: 12,
        },
        {
          login: "allenap",
          contributions: 9,
        },
        {
          login: "palfrey",
          contributions: 9,
        },
        {
          login: "Starz0r",
          contributions: 7,
        },
        {
          login: "arkadiuszbielewicz",
          contributions: 6,
        },
        {
          login: "fabricio7p",
          contributions: 6,
        },
        {
          login: "poita66",
          contributions: 6,
        },
        {
          login: "cat12079801",
          contributions: 6,
        },
        {
          login: "AnInternetTroll",
          contributions: 5,
        },
        {
          login: "boxdot",
          contributions: 5,
        },
        {
          login: "aldhsu",
          contributions: 5,
        },
        {
          login: "Gasu16",
          contributions: 4,
        },
        {
          login: "kkk669",
          contributions: 4,
        },
        {
          login: "thammin",
          contributions: 4,
        },
        {
          login: "robole",
          contributions: 4,
        },
        {
          login: "LHY-iS-Learning",
          contributions: 4,
        },
      ],
    },
    {
      name: "actix",
      stars: 6853,
      stargazers_count: 6853,

      owner: {
        login: "actix",
      },
      repo: "actix",
      contributors: [
        {
          login: "fafhrd91",
          contributions: 611,
        },
        {
          login: "JohnTitor",
          contributions: 74,
        },
        {
          login: "robjtede",
          contributions: 45,
        },
        {
          login: "DoumanAsh",
          contributions: 30,
        },
        {
          login: "fakeshadow",
          contributions: 25,
        },
        {
          login: "Jonathas-Conceicao",
          contributions: 15,
        },
        {
          login: "ava57r",
          contributions: 12,
        },
        {
          login: "cerceris",
          contributions: 11,
        },
        {
          login: "kingoflolz",
          contributions: 10,
        },
        {
          login: "Aaron1011",
          contributions: 9,
        },
        {
          login: "najamelan",
          contributions: 6,
        },
        {
          login: "rotty",
          contributions: 6,
        },
        {
          login: "GuillaumeGomez",
          contributions: 5,
        },
        {
          login: "rivertam",
          contributions: 4,
        },
        {
          login: "Dylan-DPC",
          contributions: 4,
        },
        {
          login: "bcmcmill",
          contributions: 4,
        },
        {
          login: "Xorlev",
          contributions: 3,
        },
        {
          login: "ajsyp",
          contributions: 2,
        },
        {
          login: "davidarmstronglewis",
          contributions: 2,
        },
        {
          login: "Firstyear",
          contributions: 2,
        },
        {
          login: "hoodie",
          contributions: 2,
        },
        {
          login: "kellytk",
          contributions: 2,
        },
        {
          login: "vmalloc",
          contributions: 2,
        },
        {
          login: "Sushisource",
          contributions: 2,
        },
        {
          login: "leizzer",
          contributions: 2,
        },
      ],
    },
    {
      name: "bandwhich",
      stars: 6845,
      stargazers_count: 6845,

      owner: {
        login: "imsnif",
      },
      repo: "bandwhich",
      contributors: [
        {
          login: "imsnif",
          contributions: 221,
        },
        {
          login: "zhangxp1998",
          contributions: 39,
        },
        {
          login: "ebroto",
          contributions: 24,
        },
        {
          login: "TheLostLambda",
          contributions: 7,
        },
        {
          login: "Ma27",
          contributions: 6,
        },
        {
          login: "bigtoast",
          contributions: 5,
        },
        {
          login: "remgodow",
          contributions: 4,
        },
        {
          login: "eminence",
          contributions: 3,
        },
        {
          login: "etoledom",
          contributions: 3,
        },
        {
          login: "Calinou",
          contributions: 2,
        },
        {
          login: "chobeat",
          contributions: 2,
        },
        {
          login: "sigmaSd",
          contributions: 2,
        },
        {
          login: "tim77",
          contributions: 1,
        },
        {
          login: "clux",
          contributions: 1,
        },
        {
          login: "jcgruenhage",
          contributions: 1,
        },
        {
          login: "Louis-Lesage",
          contributions: 1,
        },
        {
          login: "LoyVanBeek",
          contributions: 1,
        },
        {
          login: "thepacketgeek",
          contributions: 1,
        },
        {
          login: "MatthieuBizien",
          contributions: 1,
        },
        {
          login: "Nudin",
          contributions: 1,
        },
        {
          login: "mmstick",
          contributions: 1,
        },
        {
          login: "ncfavier",
          contributions: 1,
        },
        {
          login: "olehs0",
          contributions: 1,
        },
        {
          login: "Br1ght0ne",
          contributions: 1,
        },
        {
          login: "thedrow",
          contributions: 1,
        },
      ],
    },
    {
      name: "egui",
      stars: 6827,
      stargazers_count: 6827,

      owner: {
        login: "emilk",
      },
      repo: "egui",
      contributors: [
        {
          login: "emilk",
          contributions: 1592,
        },
        {
          login: "EmbersArc",
          contributions: 12,
        },
        {
          login: "parasyte",
          contributions: 8,
        },
        {
          login: "mankinskin",
          contributions: 5,
        },
        {
          login: "t18b219k",
          contributions: 5,
        },
        {
          login: "lampsitter",
          contributions: 5,
        },
        {
          login: "optozorax",
          contributions: 5,
        },
        {
          login: "follower",
          contributions: 4,
        },
        {
          login: "lucaspoffo",
          contributions: 4,
        },
        {
          login: "zu1k",
          contributions: 4,
        },
        {
          login: "n2",
          contributions: 4,
        },
        {
          login: "DrOptix",
          contributions: 3,
        },
        {
          login: "Bromeon",
          contributions: 3,
        },
        {
          login: "phoglund",
          contributions: 3,
        },
        {
          login: "sumibi-yakitori",
          contributions: 3,
        },
        {
          login: "AsmPrgmC3",
          contributions: 2,
        },
        {
          login: "BctfN0HUK7Yg",
          contributions: 2,
        },
        {
          login: "baysmith",
          contributions: 2,
        },
        {
          login: "quadruple-output",
          contributions: 2,
        },
        {
          login: "juancampa",
          contributions: 2,
        },
        {
          login: "katyo",
          contributions: 2,
        },
        {
          login: "paulshen",
          contributions: 2,
        },
        {
          login: "zeroeightysix",
          contributions: 2,
        },
        {
          login: "samsamai",
          contributions: 2,
        },
        {
          login: "spersson",
          contributions: 2,
        },
      ],
    },
    {
      name: "citybound",
      stars: 6703,
      stargazers_count: 6703,

      owner: {
        login: "citybound",
      },
      repo: "citybound",
      contributors: [
        {
          login: "aeplay",
          contributions: 956,
        },
        {
          login: "bogdad",
          contributions: 85,
        },
        {
          login: "o01eg",
          contributions: 36,
        },
        {
          login: "kingoflolz",
          contributions: 19,
        },
        {
          login: "Herbstein",
          contributions: 14,
        },
        {
          login: "martinrlilja",
          contributions: 4,
        },
        {
          login: "Idonoghue",
          contributions: 3,
        },
        {
          login: "NeilW",
          contributions: 3,
        },
        {
          login: "chances",
          contributions: 2,
        },
        {
          login: "manuel3108",
          contributions: 2,
        },
        {
          login: "Paul-E",
          contributions: 2,
        },
        {
          login: "SimplyNaOH",
          contributions: 2,
        },
        {
          login: "dependabot[bot]",
          contributions: 2,
        },
        {
          login: "bkircher",
          contributions: 1,
        },
        {
          login: "chdudek",
          contributions: 1,
        },
        {
          login: "wopian",
          contributions: 1,
        },
        {
          login: "beefsack",
          contributions: 1,
        },
        {
          login: "shanehandley",
          contributions: 1,
        },
        {
          login: "sseemayer",
          contributions: 1,
        },
        {
          login: "tsteinholz",
          contributions: 1,
        },
      ],
    },
    {
      name: "wasmtime",
      stars: 6672,
      stargazers_count: 6672,

      owner: {
        login: "bytecodealliance",
      },
      repo: "wasmtime",
      contributors: [
        {
          login: "sunfishcode",
          contributions: 1523,
        },
        {
          login: "pchickey",
          contributions: 788,
        },
        {
          login: "alexcrichton",
          contributions: 656,
        },
        {
          login: "fitzgen",
          contributions: 585,
        },
        {
          login: "cfallin",
          contributions: 568,
        },
        {
          login: "abrown",
          contributions: 456,
        },
        {
          login: "bnjbvr",
          contributions: 436,
        },
        {
          login: "bjorn3",
          contributions: 293,
        },
        {
          login: "peterhuene",
          contributions: 189,
        },
        {
          login: "kubkon",
          contributions: 124,
        },
        {
          login: "Vurich",
          contributions: 122,
        },
        {
          login: "yurydelendik",
          contributions: 109,
        },
        {
          login: "usize",
          contributions: 94,
        },
        {
          login: "marmistrz",
          contributions: 86,
        },
        {
          login: "afonso360",
          contributions: 76,
        },
        {
          login: "jlb6740",
          contributions: 71,
        },
        {
          login: "stoklund",
          contributions: 62,
        },
        {
          login: "tyler",
          contributions: 61,
        },
        {
          login: "angusholder",
          contributions: 47,
        },
        {
          login: "nbp",
          contributions: 46,
        },
        {
          login: "pepyakin",
          contributions: 45,
        },
        {
          login: "uweigand",
          contributions: 40,
        },
        {
          login: "akirilov-arm",
          contributions: 39,
        },
        {
          login: "dheaton-arm",
          contributions: 35,
        },
        {
          login: "mrowqa",
          contributions: 34,
        },
      ],
    },
    {
      name: "fnm",
      stars: 6631,
      stargazers_count: 6631,

      owner: {
        login: "Schniz",
      },
      repo: "fnm",
      contributors: [
        {
          login: "Schniz",
          contributions: 195,
        },
        {
          login: "renovate[bot]",
          contributions: 131,
        },
        {
          login: "tatchi",
          contributions: 9,
        },
        {
          login: "waldyrious",
          contributions: 7,
        },
        {
          login: "dependabot[bot]",
          contributions: 7,
        },
        {
          login: "thomsj",
          contributions: 6,
        },
        {
          login: "jameschensmith",
          contributions: 3,
        },
        {
          login: "pavelloz",
          contributions: 3,
        },
        {
          login: "AlexMunoz",
          contributions: 2,
        },
        {
          login: "vladimyr",
          contributions: 2,
        },
        {
          login: "dnaka91",
          contributions: 2,
        },
        {
          login: "jaythomas",
          contributions: 2,
        },
        {
          login: "pckilgore",
          contributions: 2,
        },
        {
          login: "elliottsj",
          contributions: 2,
        },
        {
          login: "ulrikstrid",
          contributions: 2,
        },
        {
          login: "scadu",
          contributions: 2,
        },
        {
          login: "pfiaux",
          contributions: 2,
        },
        {
          login: "0xflotus",
          contributions: 1,
        },
        {
          login: "Lunchb0ne",
          contributions: 1,
        },
        {
          login: "AdamGS",
          contributions: 1,
        },
        {
          login: "idkjs",
          contributions: 1,
        },
        {
          login: "amitdahan",
          contributions: 1,
        },
        {
          login: "binhonglee",
          contributions: 1,
        },
        {
          login: "binyamin",
          contributions: 1,
        },
        {
          login: "bjornua",
          contributions: 1,
        },
      ],
    },
    {
      name: "hexyl",
      stars: 6559,
      stargazers_count: 6559,

      owner: {
        login: "sharkdp",
      },
      repo: "hexyl",
      contributors: [
        {
          login: "sharkdp",
          contributions: 117,
        },
        {
          login: "ErichDonGubler",
          contributions: 17,
        },
        {
          login: "merkrafter",
          contributions: 13,
        },
        {
          login: "tommilligan",
          contributions: 7,
        },
        {
          login: "lilyball",
          contributions: 5,
        },
        {
          login: "arnavb",
          contributions: 5,
        },
        {
          login: "selfup",
          contributions: 4,
        },
        {
          login: "awidegreen",
          contributions: 3,
        },
        {
          login: "Tarnadas",
          contributions: 3,
        },
        {
          login: "a1346054",
          contributions: 3,
        },
        {
          login: "purveshpatel511",
          contributions: 3,
        },
        {
          login: "attenuation",
          contributions: 2,
        },
        {
          login: "scimas",
          contributions: 2,
        },
        {
          login: "Qyriad",
          contributions: 2,
        },
        {
          login: "PewZ",
          contributions: 2,
        },
        {
          login: "bennetthardwick",
          contributions: 1,
        },
        {
          login: "dmke",
          contributions: 1,
        },
        {
          login: "erjanmx",
          contributions: 1,
        },
        {
          login: "foo-dogsquared",
          contributions: 1,
        },
        {
          login: "Hrxn",
          contributions: 1,
        },
        {
          login: "herbygillot",
          contributions: 1,
        },
        {
          login: "issaclin32",
          contributions: 1,
        },
        {
          login: "nalshihabi",
          contributions: 1,
        },
        {
          login: "Byron",
          contributions: 1,
        },
        {
          login: "TheDoctor314",
          contributions: 1,
        },
      ],
    },
    {
      name: "broot",
      stars: 6538,
      stargazers_count: 6538,

      owner: {
        login: "Canop",
      },
      repo: "broot",
      contributors: [
        {
          login: "Canop",
          contributions: 1127,
        },
        {
          login: "lovasoa",
          contributions: 11,
        },
        {
          login: "Lucretiel",
          contributions: 8,
        },
        {
          login: "Stargateur",
          contributions: 6,
        },
        {
          login: "0xIDANT",
          contributions: 3,
        },
        {
          login: "roshan",
          contributions: 3,
        },
        {
          login: "akx",
          contributions: 2,
        },
        {
          login: "kw-andy",
          contributions: 2,
        },
        {
          login: "FabulousCupcake",
          contributions: 2,
        },
        {
          login: "guillaumecherel",
          contributions: 2,
        },
        {
          login: "herbygillot",
          contributions: 2,
        },
        {
          login: "max-sixty",
          contributions: 2,
        },
        {
          login: "rgwood",
          contributions: 2,
        },
        {
          login: "asdf8dfafjk",
          contributions: 2,
        },
        {
          login: "0xflotus",
          contributions: 1,
        },
        {
          login: "allonhadaya",
          contributions: 1,
        },
        {
          login: "azdavis",
          contributions: 1,
        },
        {
          login: "arnej",
          contributions: 1,
        },
        {
          login: "benwaffle",
          contributions: 1,
        },
        {
          login: "drahnr",
          contributions: 1,
        },
        {
          login: "chriszarate",
          contributions: 1,
        },
        {
          login: "dbast",
          contributions: 1,
        },
        {
          login: "dghaehre",
          contributions: 1,
        },
        {
          login: "vladimyr",
          contributions: 1,
        },
        {
          login: "ralt",
          contributions: 1,
        },
      ],
    },
    {
      name: "parity-ethereum",
      stars: 6529,
      stargazers_count: 6529,

      owner: {
        login: "openethereum",
      },
      repo: "parity-ethereum",
      contributors: [
        {
          login: "debris",
          contributions: 1575,
        },
        {
          login: "jacogr",
          contributions: 1489,
        },
        {
          login: "gavofyork",
          contributions: 1481,
        },
        {
          login: "tomusdrw",
          contributions: 1367,
        },
        {
          login: "arkpar",
          contributions: 1096,
        },
        {
          login: "NikVolf",
          contributions: 1031,
        },
        {
          login: "rphmeier",
          contributions: 973,
        },
        {
          login: "ngotchac",
          contributions: 385,
        },
        {
          login: "General-Beck",
          contributions: 291,
        },
        {
          login: "derhuerst",
          contributions: 180,
        },
        {
          login: "svyatonik",
          contributions: 156,
        },
        {
          login: "5chdn",
          contributions: 139,
        },
        {
          login: "niklasad1",
          contributions: 132,
        },
        {
          login: "sorpaas",
          contributions: 131,
        },
        {
          login: "dvdplm",
          contributions: 99,
        },
        {
          login: "maciejhirsz",
          contributions: 72,
        },
        {
          login: "andresilva",
          contributions: 64,
        },
        {
          login: "ordian",
          contributions: 63,
        },
        {
          login: "grbIzl",
          contributions: 57,
        },
        {
          login: "tomaka",
          contributions: 52,
        },
        {
          login: "jesuscript",
          contributions: 40,
        },
        {
          login: "0x7CFE",
          contributions: 28,
        },
        {
          login: "CraigglesO",
          contributions: 27,
        },
        {
          login: "ascjones",
          contributions: 26,
        },
        {
          login: "seunlanlege",
          contributions: 26,
        },
      ],
    },
    {
      name: "substrate",
      stars: 6480,
      stargazers_count: 6480,

      owner: {
        login: "paritytech",
      },
      repo: "substrate",
      contributors: [
        {
          login: "gavofyork",
          contributions: 675,
        },
        {
          login: "bkchr",
          contributions: 623,
        },
        {
          login: "tomaka",
          contributions: 540,
        },
        {
          login: "rphmeier",
          contributions: 281,
        },
        {
          login: "andresilva",
          contributions: 257,
        },
        {
          login: "thiolliere",
          contributions: 253,
        },
        {
          login: "arkpar",
          contributions: 223,
        },
        {
          login: "shawntabrizi",
          contributions: 199,
        },
        {
          login: "kianenigma",
          contributions: 195,
        },
        {
          login: "tomusdrw",
          contributions: 191,
        },
        {
          login: "pepyakin",
          contributions: 163,
        },
        {
          login: "mxinden",
          contributions: 124,
        },
        {
          login: "NikVolf",
          contributions: 120,
        },
        {
          login: "gnunicorn",
          contributions: 120,
        },
        {
          login: "athei",
          contributions: 110,
        },
        {
          login: "dependabot[bot]",
          contributions: 108,
        },
        {
          login: "sorpaas",
          contributions: 103,
        },
        {
          login: "svyatonik",
          contributions: 90,
        },
        {
          login: "xlc",
          contributions: 73,
        },
        {
          login: "TriplEight",
          contributions: 70,
        },
        {
          login: "expenses",
          contributions: 68,
        },
        {
          login: "marcio-diaz",
          contributions: 56,
        },
        {
          login: "cecton",
          contributions: 53,
        },
        {
          login: "stanislav-tkach",
          contributions: 50,
        },
        {
          login: "cheme",
          contributions: 47,
        },
      ],
    },
    {
      name: "abstreet",
      stars: 6407,
      stargazers_count: 6407,

      owner: {
        login: "a-b-street",
      },
      repo: "abstreet",
      contributors: [
        {
          login: "dabreegster",
          contributions: 6979,
        },
        {
          login: "michaelkirk",
          contributions: 291,
        },
        {
          login: "dcarlino",
          contributions: 18,
        },
        {
          login: "matkoniecz",
          contributions: 11,
        },
        {
          login: "gedkott",
          contributions: 8,
        },
        {
          login: "BruceBrown",
          contributions: 7,
        },
        {
          login: "RestitutorOrbis",
          contributions: 6,
        },
        {
          login: "tnederlof",
          contributions: 6,
        },
        {
          login: "vks",
          contributions: 6,
        },
        {
          login: "Robinlovelace",
          contributions: 3,
        },
        {
          login: "mdejean",
          contributions: 2,
        },
        {
          login: "omalaspinas",
          contributions: 2,
        },
        {
          login: "remilauzier",
          contributions: 2,
        },
        {
          login: "NoSuchThingAsRandom",
          contributions: 2,
        },
        {
          login: "nowei",
          contributions: 1,
        },
        {
          login: "br-lewis",
          contributions: 1,
        },
        {
          login: "crschmidt",
          contributions: 1,
        },
        {
          login: "dmitriy-serdyuk",
          contributions: 1,
        },
        {
          login: "starsep",
          contributions: 1,
        },
        {
          login: "Fullstop000",
          contributions: 1,
        },
        {
          login: "jraymakers",
          contributions: 1,
        },
        {
          login: "jvolker",
          contributions: 1,
        },
        {
          login: "khuston",
          contributions: 1,
        },
        {
          login: "lorenzschmid",
          contributions: 1,
        },
        {
          login: "mshenfield",
          contributions: 1,
        },
      ],
    },
    {
      name: "tui-rs",
      stars: 6339,
      stargazers_count: 6339,

      owner: {
        login: "fdehau",
      },
      repo: "tui-rs",
      contributors: [
        {
          login: "fdehau",
          contributions: 288,
        },
        {
          login: "karolinepauls",
          contributions: 13,
        },
        {
          login: "TimonPost",
          contributions: 9,
        },
        {
          login: "abusch",
          contributions: 8,
        },
        {
          login: "defiori",
          contributions: 8,
        },
        {
          login: "dflemstr",
          contributions: 5,
        },
        {
          login: "stevensonmt",
          contributions: 5,
        },
        {
          login: "extrawurst",
          contributions: 5,
        },
        {
          login: "TheLostLambda",
          contributions: 4,
        },
        {
          login: "cjbassi",
          contributions: 4,
        },
        {
          login: "sectore",
          contributions: 4,
        },
        {
          login: "JosephFKnight",
          contributions: 4,
        },
        {
          login: "man0lis",
          contributions: 3,
        },
        {
          login: "wose",
          contributions: 3,
        },
        {
          login: "svenstaro",
          contributions: 3,
        },
        {
          login: "scauligi",
          contributions: 3,
        },
        {
          login: "scvalex",
          contributions: 2,
        },
        {
          login: "eminence",
          contributions: 2,
        },
        {
          login: "imsnif",
          contributions: 2,
        },
        {
          login: "ClementTsang",
          contributions: 2,
        },
        {
          login: "deepu105",
          contributions: 2,
        },
        {
          login: "jeffa5",
          contributions: 2,
        },
        {
          login: "z2oh",
          contributions: 2,
        },
        {
          login: "nebkor",
          contributions: 2,
        },
        {
          login: "Kemyt",
          contributions: 2,
        },
      ],
    },
    {
      name: "neon",
      stars: 6328,
      stargazers_count: 6328,

      owner: {
        login: "neon-bindings",
      },
      repo: "neon",
      contributors: [
        {
          login: "dherman",
          contributions: 417,
        },
        {
          login: "kjvalencik",
          contributions: 219,
        },
        {
          login: "goto-bus-stop",
          contributions: 99,
        },
        {
          login: "amilajack",
          contributions: 60,
        },
        {
          login: "jedireza",
          contributions: 37,
        },
        {
          login: "dependabot[bot]",
          contributions: 31,
        },
        {
          login: "mfpiccolo",
          contributions: 30,
        },
        {
          login: "lhr0909",
          contributions: 13,
        },
        {
          login: "maxbrunsfeld",
          contributions: 10,
        },
        {
          login: "corbinu",
          contributions: 7,
        },
        {
          login: "watilde",
          contributions: 7,
        },
        {
          login: "mmun",
          contributions: 7,
        },
        {
          login: "patr0nus",
          contributions: 7,
        },
        {
          login: "MilanLoveless",
          contributions: 6,
        },
        {
          login: "hone",
          contributions: 6,
        },
        {
          login: "sbillig",
          contributions: 6,
        },
        {
          login: "GabrielCastro",
          contributions: 4,
        },
        {
          login: "geovie",
          contributions: 4,
        },
        {
          login: "usagi",
          contributions: 4,
        },
        {
          login: "dbkr",
          contributions: 3,
        },
        {
          login: "eddyb",
          contributions: 3,
        },
        {
          login: "jrose-signal",
          contributions: 3,
        },
        {
          login: "ogoffart",
          contributions: 3,
        },
        {
          login: "ffflorian",
          contributions: 3,
        },
        {
          login: "matklad",
          contributions: 2,
        },
      ],
    },
    {
      name: "psst",
      stars: 6311,
      stargazers_count: 6311,

      owner: {
        login: "jpochyla",
      },
      repo: "psst",
      contributors: [
        {
          login: "jpochyla",
          contributions: 341,
        },
        {
          login: "martingoe",
          contributions: 7,
        },
        {
          login: "gordonshieh",
          contributions: 7,
        },
        {
          login: "Kethku",
          contributions: 5,
        },
        {
          login: "Kesefon",
          contributions: 5,
        },
        {
          login: "gokayokyay",
          contributions: 4,
        },
        {
          login: "JuliDi",
          contributions: 3,
        },
        {
          login: "reynn",
          contributions: 3,
        },
        {
          login: "paolobarbolini",
          contributions: 3,
        },
        {
          login: "brightly-salty",
          contributions: 2,
        },
        {
          login: "klemensn",
          contributions: 2,
        },
        {
          login: "Liberatys",
          contributions: 2,
        },
        {
          login: "soooch",
          contributions: 2,
        },
        {
          login: "timgws",
          contributions: 2,
        },
        {
          login: "asnelling",
          contributions: 1,
        },
        {
          login: "zrthxn",
          contributions: 1,
        },
        {
          login: "aeons",
          contributions: 1,
        },
        {
          login: "guerinoni",
          contributions: 1,
        },
        {
          login: "iandwelker",
          contributions: 1,
        },
        {
          login: "jstarpl",
          contributions: 1,
        },
        {
          login: "lonkle",
          contributions: 1,
        },
        {
          login: "im-coder-lg",
          contributions: 1,
        },
        {
          login: "brandly",
          contributions: 1,
        },
        {
          login: "paulfariello-syn",
          contributions: 1,
        },
        {
          login: "stared",
          contributions: 1,
        },
      ],
    },
    {
      name: "nom",
      stars: 6165,
      stargazers_count: 6165,

      owner: {
        login: "Geal",
      },
      repo: "nom",
      contributors: [
        {
          login: "Geal",
          contributions: 1784,
        },
        {
          login: "Keruspe",
          contributions: 65,
        },
        {
          login: "tstorch",
          contributions: 57,
        },
        {
          login: "nickelc",
          contributions: 26,
        },
        {
          login: "joelself",
          contributions: 21,
        },
        {
          login: "kamarkiewicz",
          contributions: 20,
        },
        {
          login: "NamsooCho",
          contributions: 19,
        },
        {
          login: "sourrust",
          contributions: 15,
        },
        {
          login: "kpp",
          contributions: 15,
        },
        {
          login: "homersimpsons",
          contributions: 14,
        },
        {
          login: "Hywan",
          contributions: 13,
        },
        {
          login: "chifflier",
          contributions: 12,
        },
        {
          login: "Lucretiel",
          contributions: 11,
        },
        {
          login: "GuillaumeGomez",
          contributions: 10,
        },
        {
          login: "juchiast",
          contributions: 8,
        },
        {
          login: "derekdreery",
          contributions: 8,
        },
        {
          login: "jrakow",
          contributions: 7,
        },
        {
          login: "jansegre",
          contributions: 6,
        },
        {
          login: "Waelwindows",
          contributions: 6,
        },
        {
          login: "CAD97",
          contributions: 5,
        },
        {
          login: "meh",
          contributions: 5,
        },
        {
          login: "badboy",
          contributions: 5,
        },
        {
          login: "xfix",
          contributions: 5,
        },
        {
          login: "lu-zero",
          contributions: 5,
        },
        {
          login: "ayrat555",
          contributions: 4,
        },
      ],
    },
    {
      name: "rayon",
      stars: 6129,
      stargazers_count: 6129,

      owner: {
        login: "rayon-rs",
      },
      repo: "rayon",
      contributors: [
        {
          login: "nikomatsakis",
          contributions: 616,
        },
        {
          login: "cuviper",
          contributions: 586,
        },
        {
          login: "bors[bot]",
          contributions: 165,
        },
        {
          login: "Kerollmops",
          contributions: 40,
        },
        {
          login: "schuster",
          contributions: 19,
        },
        {
          login: "leoyvens",
          contributions: 17,
        },
        {
          login: "ChristopherDavenport",
          contributions: 17,
        },
        {
          login: "tmccombs",
          contributions: 15,
        },
        {
          login: "brendanzab",
          contributions: 11,
        },
        {
          login: "bluss",
          contributions: 11,
        },
        {
          login: "jwass",
          contributions: 10,
        },
        {
          login: "willi-kappler",
          contributions: 9,
        },
        {
          login: "Amanieu",
          contributions: 8,
        },
        {
          login: "QuietMisdreavus",
          contributions: 8,
        },
        {
          login: "ralfbiedert",
          contributions: 8,
        },
        {
          login: "HadrienG2",
          contributions: 7,
        },
        {
          login: "CAD97",
          contributions: 6,
        },
        {
          login: "ignatenkobrain",
          contributions: 6,
        },
        {
          login: "AndyGauge",
          contributions: 5,
        },
        {
          login: "edre",
          contributions: 5,
        },
        {
          login: "huonw",
          contributions: 5,
        },
        {
          login: "seanchen1991",
          contributions: 5,
        },
        {
          login: "oddg",
          contributions: 5,
        },
        {
          login: "chrisvittal",
          contributions: 4,
        },
        {
          login: "dtolnay",
          contributions: 4,
        },
      ],
    },
    {
      name: "windows-rs",
      stars: 6121,
      stargazers_count: 6121,

      owner: {
        login: "microsoft",
      },
      repo: "windows-rs",
      contributors: [
        {
          login: "kennykerr",
          contributions: 525,
        },
        {
          login: "rylev",
          contributions: 102,
        },
        {
          login: "MarijnS95",
          contributions: 18,
        },
        {
          login: "riverar",
          contributions: 16,
        },
        {
          login: "zachlute",
          contributions: 7,
        },
        {
          login: "microsoftopensource",
          contributions: 4,
        },
        {
          login: "robmikh",
          contributions: 4,
        },
        {
          login: "tim-weis",
          contributions: 3,
        },
        {
          login: "yoshuawuyts",
          contributions: 3,
        },
        {
          login: "wravery",
          contributions: 2,
        },
        {
          login: "damyanp",
          contributions: 2,
        },
        {
          login: "GuillaumeGomez",
          contributions: 2,
        },
        {
          login: "roblabla",
          contributions: 2,
        },
        {
          login: "bdbai",
          contributions: 2,
        },
        {
          login: "abdulniyaspm",
          contributions: 1,
        },
        {
          login: "ajeetdsouza",
          contributions: 1,
        },
        {
          login: "Alovchin91",
          contributions: 1,
        },
        {
          login: "astraw",
          contributions: 1,
        },
        {
          login: "arturdryomov",
          contributions: 1,
        },
        {
          login: "complexspaces",
          contributions: 1,
        },
        {
          login: "danielframpton",
          contributions: 1,
        },
        {
          login: "DoctorNefario",
          contributions: 1,
        },
        {
          login: "skyfloogle",
          contributions: 1,
        },
        {
          login: "nbdd0121",
          contributions: 1,
        },
        {
          login: "Guiguiprim",
          contributions: 1,
        },
      ],
    },
    {
      name: "tokei",
      stars: 6115,
      stargazers_count: 6115,

      owner: {
        login: "XAMPPRocky",
      },
      repo: "tokei",
      contributors: [
        {
          login: "XAMPPRocky",
          contributions: 491,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 143,
        },
        {
          login: "NickHackman",
          contributions: 13,
        },
        {
          login: "tuncer",
          contributions: 8,
        },
        {
          login: "dependabot[bot]",
          contributions: 6,
        },
        {
          login: "alexmaco",
          contributions: 6,
        },
        {
          login: "LovecraftianHorror",
          contributions: 5,
        },
        {
          login: "brightly-salty",
          contributions: 4,
        },
        {
          login: "liigo",
          contributions: 4,
        },
        {
          login: "BrandonBoone",
          contributions: 3,
        },
        {
          login: "fkarg",
          contributions: 3,
        },
        {
          login: "Veykril",
          contributions: 3,
        },
        {
          login: "llogiq",
          contributions: 3,
        },
        {
          login: "tjodden",
          contributions: 2,
        },
        {
          login: "lespea",
          contributions: 2,
        },
        {
          login: "DanteFalzone0",
          contributions: 2,
        },
        {
          login: "dtolnay",
          contributions: 2,
        },
        {
          login: "pickfire",
          contributions: 2,
        },
        {
          login: "Janfel",
          contributions: 2,
        },
        {
          login: "jespino",
          contributions: 2,
        },
        {
          login: "kaushalmodi",
          contributions: 2,
        },
        {
          login: "lpil",
          contributions: 2,
        },
        {
          login: "Luthaf",
          contributions: 2,
        },
        {
          login: "mwilli20",
          contributions: 2,
        },
        {
          login: "Byron",
          contributions: 2,
        },
      ],
    },
    {
      name: "czkawka",
      stars: 6076,
      stargazers_count: 6076,

      owner: {
        login: "qarmin",
      },
      repo: "czkawka",
      contributors: [
        {
          login: "qarmin",
          contributions: 418,
        },
        {
          login: "alexislefebvre",
          contributions: 8,
        },
        {
          login: "blob79",
          contributions: 7,
        },
        {
          login: "pczarn",
          contributions: 4,
        },
        {
          login: "bbodenmiller",
          contributions: 2,
        },
        {
          login: "dandv",
          contributions: 2,
        },
        {
          login: "igor-cali",
          contributions: 2,
        },
        {
          login: "ShrirajHegde",
          contributions: 2,
        },
        {
          login: "krzysdz",
          contributions: 2,
        },
        {
          login: "TheEvilSkeleton",
          contributions: 2,
        },
        {
          login: "0xflotus",
          contributions: 1,
        },
        {
          login: "procrastinationfighter",
          contributions: 1,
        },
        {
          login: "Caduser2020",
          contributions: 1,
        },
        {
          login: "DannyJJK",
          contributions: 1,
        },
        {
          login: "darksv",
          contributions: 1,
        },
        {
          login: "DoumanAsh",
          contributions: 1,
        },
        {
          login: "Elfein7Night",
          contributions: 1,
        },
        {
          login: "Farmadupe",
          contributions: 1,
        },
        {
          login: "habernal",
          contributions: 1,
        },
        {
          login: "janjur",
          contributions: 1,
        },
        {
          login: "jonadem",
          contributions: 1,
        },
        {
          login: "jhult",
          contributions: 1,
        },
        {
          login: "MeirKlemp",
          contributions: 1,
        },
        {
          login: "Mek101",
          contributions: 1,
        },
        {
          login: "michaelgrigoryan25",
          contributions: 1,
        },
      ],
    },
    {
      name: "neovide",
      stars: 6051,
      stargazers_count: 6051,

      owner: {
        login: "neovide",
      },
      repo: "neovide",
      contributors: [
        {
          login: "Kethku",
          contributions: 411,
        },
        {
          login: "last-partizan",
          contributions: 60,
        },
        {
          login: "jonvaldes",
          contributions: 53,
        },
        {
          login: "keithsim-msft",
          contributions: 41,
        },
        {
          login: "LoipesMas",
          contributions: 31,
        },
        {
          login: "j4qfrost",
          contributions: 27,
        },
        {
          login: "exoticus",
          contributions: 17,
        },
        {
          login: "pushqrdx",
          contributions: 13,
        },
        {
          login: "shaunsingh",
          contributions: 13,
        },
        {
          login: "vzex",
          contributions: 8,
        },
        {
          login: "fredizzimo",
          contributions: 8,
        },
        {
          login: "pawlowskialex",
          contributions: 7,
        },
        {
          login: "RMichelsen",
          contributions: 7,
        },
        {
          login: "nganhkhoa",
          contributions: 6,
        },
        {
          login: "KillTheMule",
          contributions: 5,
        },
        {
          login: "Benjamin-Davies",
          contributions: 4,
        },
        {
          login: "smolck",
          contributions: 4,
        },
        {
          login: "rahuliyer95",
          contributions: 4,
        },
        {
          login: "lf-",
          contributions: 3,
        },
        {
          login: "lucas-miranda",
          contributions: 3,
        },
        {
          login: "luisholanda",
          contributions: 3,
        },
        {
          login: "PyGamer0",
          contributions: 3,
        },
        {
          login: "p00f",
          contributions: 3,
        },
        {
          login: "clason",
          contributions: 3,
        },
        {
          login: "jsosulski",
          contributions: 3,
        },
      ],
    },
    {
      name: "bottlerocket",
      stars: 6047,
      stargazers_count: 6047,

      owner: {
        login: "bottlerocket-os",
      },
      repo: "bottlerocket",
      contributors: [
        {
          login: "tjkirch",
          contributions: 965,
        },
        {
          login: "bcressey",
          contributions: 656,
        },
        {
          login: "iliana",
          contributions: 407,
        },
        {
          login: "etungsten",
          contributions: 402,
        },
        {
          login: "zmrow",
          contributions: 260,
        },
        {
          login: "jahkeup",
          contributions: 197,
        },
        {
          login: "arnaldo2792",
          contributions: 124,
        },
        {
          login: "webern",
          contributions: 123,
        },
        {
          login: "samuelkarp",
          contributions: 119,
        },
        {
          login: "sam-aws",
          contributions: 108,
        },
        {
          login: "jpculp",
          contributions: 98,
        },
        {
          login: "cbgbt",
          contributions: 62,
        },
        {
          login: "gthao313",
          contributions: 39,
        },
        {
          login: "jamieand",
          contributions: 23,
        },
        {
          login: "sanu11",
          contributions: 20,
        },
        {
          login: "patraw",
          contributions: 13,
        },
        {
          login: "srgothi92",
          contributions: 8,
        },
        {
          login: "WilboMo",
          contributions: 5,
        },
        {
          login: "samjo-nyang",
          contributions: 4,
        },
        {
          login: "felipeac",
          contributions: 3,
        },
        {
          login: "gregdek",
          contributions: 3,
        },
        {
          login: "jhaynes",
          contributions: 2,
        },
        {
          login: "aashnasheth",
          contributions: 2,
        },
        {
          login: "bnrjee",
          contributions: 1,
        },
        {
          login: "andrewhsu",
          contributions: 1,
        },
      ],
    },
    {
      name: "iron",
      stars: 6018,
      stargazers_count: 6018,

      owner: {
        login: "iron",
      },
      repo: "iron",
      contributors: [
        {
          login: "reem",
          contributions: 950,
        },
        {
          login: "zzmp",
          contributions: 205,
        },
        {
          login: "untitaker",
          contributions: 113,
        },
        {
          login: "phlmn",
          contributions: 65,
        },
        {
          login: "theptrk",
          contributions: 20,
        },
        {
          login: "michaelsproul",
          contributions: 19,
        },
        {
          login: "mcreinhard",
          contributions: 17,
        },
        {
          login: "SkylerLipthay",
          contributions: 17,
        },
        {
          login: "brycefisher",
          contributions: 12,
        },
        {
          login: "duelinmarkers",
          contributions: 12,
        },
        {
          login: "tomprince",
          contributions: 12,
        },
        {
          login: "carols10cents",
          contributions: 11,
        },
        {
          login: "leoyvens",
          contributions: 11,
        },
        {
          login: "emberian",
          contributions: 10,
        },
        {
          login: "sfackler",
          contributions: 10,
        },
        {
          login: "MJDSys",
          contributions: 9,
        },
        {
          login: "robinst",
          contributions: 9,
        },
        {
          login: "zgtm",
          contributions: 9,
        },
        {
          login: "panicbit",
          contributions: 8,
        },
        {
          login: "AerialX",
          contributions: 5,
        },
        {
          login: "dorfsmay",
          contributions: 5,
        },
        {
          login: "Fiedzia",
          contributions: 5,
        },
        {
          login: "gsquire",
          contributions: 5,
        },
        {
          login: "lambda-fairy",
          contributions: 4,
        },
        {
          login: "trotter",
          contributions: 4,
        },
      ],
    },
    {
      name: "warp",
      stars: 5868,
      stargazers_count: 5868,

      owner: {
        login: "seanmonstar",
      },
      repo: "warp",
      contributors: [
        {
          login: "seanmonstar",
          contributions: 328,
        },
        {
          login: "jxs",
          contributions: 13,
        },
        {
          login: "nickelc",
          contributions: 7,
        },
        {
          login: "remexre",
          contributions: 7,
        },
        {
          login: "emschwartz",
          contributions: 5,
        },
        {
          login: "SimonSapin",
          contributions: 5,
        },
        {
          login: "coolreader18",
          contributions: 4,
        },
        {
          login: "FlorianDr",
          contributions: 3,
        },
        {
          login: "sunng87",
          contributions: 3,
        },
        {
          login: "Some-Dood",
          contributions: 2,
        },
        {
          login: "cdvv7788",
          contributions: 2,
        },
        {
          login: "dmexe",
          contributions: 2,
        },
        {
          login: "hawkw",
          contributions: 2,
        },
        {
          login: "ignatenkobrain",
          contributions: 2,
        },
        {
          login: "kamalmarhubi",
          contributions: 2,
        },
        {
          login: "LucioFranco",
          contributions: 2,
        },
        {
          login: "FSMaxB",
          contributions: 2,
        },
        {
          login: "najamelan",
          contributions: 2,
        },
        {
          login: "rockwotj",
          contributions: 2,
        },
        {
          login: "boxdot",
          contributions: 2,
        },
        {
          login: "akbarpn136",
          contributions: 1,
        },
        {
          login: "Aankhen",
          contributions: 1,
        },
        {
          login: "adamchalmers",
          contributions: 1,
        },
        {
          login: "akappel",
          contributions: 1,
        },
        {
          login: "AlexCovizzi",
          contributions: 1,
        },
      ],
    },
    {
      name: "lemmy",
      stars: 5855,
      stargazers_count: 5855,

      owner: {
        login: "LemmyNet",
      },
      repo: "lemmy",
      contributors: [
        {
          login: "dessalines",
          contributions: 2297,
        },
        {
          login: "Nutomic",
          contributions: 673,
        },
        {
          login: "StaticallyTypedRice",
          contributions: 44,
        },
        {
          login: "asonix",
          contributions: 31,
        },
        {
          login: "AutomCoding",
          contributions: 18,
        },
        {
          login: "eiknat",
          contributions: 14,
        },
        {
          login: "AndreVallestero",
          contributions: 11,
        },
        {
          login: "masterstur",
          contributions: 11,
        },
        {
          login: "ernestwisniewski",
          contributions: 11,
        },
        {
          login: "zeroone2numeral2",
          contributions: 10,
        },
        {
          login: "maxigaz",
          contributions: 9,
        },
        {
          login: "adynemo",
          contributions: 8,
        },
        {
          login: "ralyodio",
          contributions: 8,
        },
        {
          login: "pojntfx",
          contributions: 8,
        },
        {
          login: "Filip785",
          contributions: 8,
        },
        {
          login: "iav",
          contributions: 8,
        },
        {
          login: "teromene",
          contributions: 7,
        },
        {
          login: "sylviaji",
          contributions: 5,
        },
        {
          login: "zacanger",
          contributions: 5,
        },
        {
          login: "xyvs",
          contributions: 5,
        },
        {
          login: "knkski",
          contributions: 5,
        },
        {
          login: "niniack",
          contributions: 5,
        },
        {
          login: "fitojb",
          contributions: 4,
        },
        {
          login: "Mart-Bogdan",
          contributions: 4,
        },
        {
          login: "commagray",
          contributions: 4,
        },
      ],
    },
    {
      name: "tantivy",
      stars: 5821,
      stargazers_count: 5821,

      owner: {
        login: "quickwit-oss",
      },
      repo: "tantivy",
      contributors: [
        {
          login: "fulmicoton",
          contributions: 1542,
        },
        {
          login: "PSeitz",
          contributions: 178,
        },
        {
          login: "lnicola",
          contributions: 54,
        },
        {
          login: "currymj",
          contributions: 25,
        },
        {
          login: "evanxg852000",
          contributions: 23,
        },
        {
          login: "dependabot[bot]",
          contributions: 20,
        },
        {
          login: "petr-tik",
          contributions: 19,
        },
        {
          login: "KodrAus",
          contributions: 18,
        },
        {
          login: "drusellers",
          contributions: 17,
        },
        {
          login: "barrotsteindev",
          contributions: 17,
        },
        {
          login: "vigneshsarma",
          contributions: 13,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 11,
        },
        {
          login: "guilload",
          contributions: 9,
        },
        {
          login: "rihardsk",
          contributions: 9,
        },
        {
          login: "jason-wolfe",
          contributions: 9,
        },
        {
          login: "trinity-1686a",
          contributions: 7,
        },
        {
          login: "fmassot",
          contributions: 6,
        },
        {
          login: "shikhar",
          contributions: 6,
        },
        {
          login: "lengyijun",
          contributions: 6,
        },
        {
          login: "appaquet",
          contributions: 5,
        },
        {
          login: "hardikpnsp",
          contributions: 4,
        },
        {
          login: "manuel-woelker",
          contributions: 4,
        },
        {
          login: "vishalsodani",
          contributions: 4,
        },
        {
          login: "k-yomo",
          contributions: 4,
        },
        {
          login: "robyoung",
          contributions: 4,
        },
      ],
    },
    {
      name: "spotifyd",
      stars: 5811,
      stargazers_count: 5811,

      owner: {
        login: "Spotifyd",
      },
      repo: "spotifyd",
      contributors: [
        {
          login: "SimonPersson",
          contributions: 159,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 43,
        },
        {
          login: "robinvd",
          contributions: 26,
        },
        {
          login: "JojiiOfficial",
          contributions: 25,
        },
        {
          login: "0jdxt",
          contributions: 14,
        },
        {
          login: "capnfabs",
          contributions: 13,
        },
        {
          login: "bcmyers",
          contributions: 10,
        },
        {
          login: "mh84",
          contributions: 9,
        },
        {
          login: "valir",
          contributions: 5,
        },
        {
          login: "Mitschmaster",
          contributions: 4,
        },
        {
          login: "Yarn",
          contributions: 3,
        },
        {
          login: "allcontributors[bot]",
          contributions: 3,
        },
        {
          login: "tommady",
          contributions: 3,
        },
        {
          login: "zv0n",
          contributions: 3,
        },
        {
          login: "nkitan",
          contributions: 2,
        },
        {
          login: "basvandenbrink",
          contributions: 2,
        },
        {
          login: "jackloughran",
          contributions: 2,
        },
        {
          login: "joelpet",
          contributions: 2,
        },
        {
          login: "juliangaal",
          contributions: 2,
        },
        {
          login: "nicokaiser",
          contributions: 2,
        },
        {
          login: "pstruschka",
          contributions: 2,
        },
        {
          login: "pfrenssen",
          contributions: 2,
        },
        {
          login: "newpavlov",
          contributions: 2,
        },
        {
          login: "Plecra",
          contributions: 2,
        },
        {
          login: "0xflotus",
          contributions: 1,
        },
      ],
    },
    {
      name: "gping",
      stars: 5753,
      stargazers_count: 5753,

      owner: {
        login: "orf",
      },
      repo: "gping",
      contributors: [
        {
          login: "orf",
          contributions: 194,
        },
        {
          login: "yabberyabber",
          contributions: 6,
        },
        {
          login: "hbina",
          contributions: 4,
        },
        {
          login: "thepacketgeek",
          contributions: 3,
        },
        {
          login: "barrowsys",
          contributions: 2,
        },
        {
          login: "kidonng",
          contributions: 2,
        },
        {
          login: "LouisBrunner",
          contributions: 2,
        },
        {
          login: "p2004a",
          contributions: 2,
        },
        {
          login: "Thynix",
          contributions: 2,
        },
        {
          login: "niutech",
          contributions: 2,
        },
        {
          login: "opexxx",
          contributions: 1,
        },
        {
          login: "adaasch",
          contributions: 1,
        },
        {
          login: "tim77",
          contributions: 1,
        },
        {
          login: "BlancRay",
          contributions: 1,
        },
        {
          login: "SOF3",
          contributions: 1,
        },
        {
          login: "vladimyr",
          contributions: 1,
        },
        {
          login: "evandandrea",
          contributions: 1,
        },
        {
          login: "Calinou",
          contributions: 1,
        },
        {
          login: "ijohanne",
          contributions: 1,
        },
        {
          login: "ImgBotApp",
          contributions: 1,
        },
        {
          login: "KSXGitHub",
          contributions: 1,
        },
        {
          login: "KyroChi",
          contributions: 1,
        },
        {
          login: "kromanenko",
          contributions: 1,
        },
        {
          login: "debMan",
          contributions: 1,
        },
        {
          login: "nickburlett",
          contributions: 1,
        },
      ],
    },
    {
      name: "quiche",
      stars: 5651,
      stargazers_count: 5651,

      owner: {
        login: "cloudflare",
      },
      repo: "quiche",
      contributors: [
        {
          login: "ghedo",
          contributions: 1144,
        },
        {
          login: "LPardue",
          contributions: 128,
        },
        {
          login: "junhochoi",
          contributions: 111,
        },
        {
          login: "RReverser",
          contributions: 14,
        },
        {
          login: "normanmaurer",
          contributions: 13,
        },
        {
          login: "lohith-bellad",
          contributions: 5,
        },
        {
          login: "kachayev",
          contributions: 4,
        },
        {
          login: "alpire",
          contributions: 3,
        },
        {
          login: "VMatrix1900",
          contributions: 3,
        },
        {
          login: "schien",
          contributions: 3,
        },
        {
          login: "ehaydenr",
          contributions: 3,
        },
        {
          login: "nox",
          contributions: 2,
        },
        {
          login: "ctiller",
          contributions: 2,
        },
        {
          login: "bagder",
          contributions: 2,
        },
        {
          login: "hguandl",
          contributions: 2,
        },
        {
          login: "omarkilani",
          contributions: 2,
        },
        {
          login: "Lekensteyn",
          contributions: 2,
        },
        {
          login: "nqv",
          contributions: 2,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 2,
        },
        {
          login: "kevinjohna6",
          contributions: 2,
        },
        {
          login: "ebi-yade",
          contributions: 2,
        },
        {
          login: "centminmod",
          contributions: 2,
        },
        {
          login: "xanathar",
          contributions: 2,
        },
        {
          login: "0xflotus",
          contributions: 1,
        },
        {
          login: "xforce",
          contributions: 1,
        },
      ],
    },
    {
      name: "druid",
      stars: 5620,
      stargazers_count: 5620,

      owner: {
        login: "linebender",
      },
      repo: "druid",
      contributors: [
        {
          login: "cmyr",
          contributions: 538,
        },
        {
          login: "raphlinus",
          contributions: 308,
        },
        {
          login: "futurepaul",
          contributions: 189,
        },
        {
          login: "xStrom",
          contributions: 108,
        },
        {
          login: "Maan2003",
          contributions: 101,
        },
        {
          login: "derekdreery",
          contributions: 85,
        },
        {
          login: "jneem",
          contributions: 84,
        },
        {
          login: "zaynetro",
          contributions: 59,
        },
        {
          login: "ForLoveOfCats",
          contributions: 55,
        },
        {
          login: "luleyleo",
          contributions: 45,
        },
        {
          login: "rylev",
          contributions: 43,
        },
        {
          login: "JAicewizard",
          contributions: 40,
        },
        {
          login: "mrandri19",
          contributions: 33,
        },
        {
          login: "vbsteven",
          contributions: 30,
        },
        {
          login: "PoignardAzur",
          contributions: 25,
        },
        {
          login: "rjwittams",
          contributions: 24,
        },
        {
          login: "rhzk",
          contributions: 24,
        },
        {
          login: "scholtzan",
          contributions: 23,
        },
        {
          login: "james-lawrence",
          contributions: 23,
        },
        {
          login: "edwin0cheng",
          contributions: 21,
        },
        {
          login: "mendelt",
          contributions: 20,
        },
        {
          login: "psychon",
          contributions: 20,
        },
        {
          login: "Ralith",
          contributions: 19,
        },
        {
          login: "Dmitry-Borodin",
          contributions: 17,
        },
        {
          login: "fishrockz",
          contributions: 15,
        },
      ],
    },
    {
      name: "helix",
      stars: 5562,
      stargazers_count: 5562,

      owner: {
        login: "helix-editor",
      },
      repo: "helix",
      contributors: [
        {
          login: "archseer",
          contributions: 972,
        },
        {
          login: "cessen",
          contributions: 111,
        },
        {
          login: "dependabot[bot]",
          contributions: 104,
        },
        {
          login: "vv9k",
          contributions: 83,
        },
        {
          login: "pickfire",
          contributions: 78,
        },
        {
          login: "sudormrfbin",
          contributions: 77,
        },
        {
          login: "janhrastnik",
          contributions: 70,
        },
        {
          login: "kirawi",
          contributions: 46,
        },
        {
          login: "Omnikar",
          contributions: 46,
        },
        {
          login: "the-mikedavis",
          contributions: 34,
        },
        {
          login: "cossonleo",
          contributions: 19,
        },
        {
          login: "WindSoilder",
          contributions: 18,
        },
        {
          login: "ath3",
          contributions: 18,
        },
        {
          login: "jasonrhansen",
          contributions: 17,
        },
        {
          login: "CBenoit",
          contributions: 15,
        },
        {
          login: "kevinsjoberg",
          contributions: 13,
        },
        {
          login: "notoria",
          contributions: 12,
        },
        {
          login: "IceDragon200",
          contributions: 10,
        },
        {
          login: "yusdacra",
          contributions: 9,
        },
        {
          login: "Flakebi",
          contributions: 9,
        },
        {
          login: "Nehliin",
          contributions: 9,
        },
        {
          login: "matoous",
          contributions: 8,
        },
        {
          login: "NNBnh",
          contributions: 8,
        },
        {
          login: "QiBaobin",
          contributions: 7,
        },
        {
          login: "dead10ck",
          contributions: 7,
        },
      ],
    },
    {
      name: "genact",
      stars: 5562,
      stargazers_count: 5562,

      owner: {
        login: "svenstaro",
      },
      repo: "genact",
      contributors: [
        {
          login: "svenstaro",
          contributions: 419,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 141,
        },
        {
          login: "dependabot[bot]",
          contributions: 81,
        },
        {
          login: "Kovah",
          contributions: 20,
        },
        {
          login: "opatut",
          contributions: 15,
        },
        {
          login: "TheIronBorn",
          contributions: 11,
        },
        {
          login: "equal-l2",
          contributions: 8,
        },
        {
          login: "xyproto",
          contributions: 3,
        },
        {
          login: "hatzel",
          contributions: 2,
        },
        {
          login: "kaxing",
          contributions: 2,
        },
        {
          login: "floj",
          contributions: 2,
        },
        {
          login: "gaga5lala",
          contributions: 2,
        },
        {
          login: "briankabiro",
          contributions: 2,
        },
        {
          login: "sno2",
          contributions: 1,
        },
        {
          login: "Crocmagnon",
          contributions: 1,
        },
        {
          login: "herbygillot",
          contributions: 1,
        },
        {
          login: "lilianmoraru",
          contributions: 1,
        },
        {
          login: "arzzen",
          contributions: 1,
        },
        {
          login: "althonos",
          contributions: 1,
        },
        {
          login: "0mp",
          contributions: 1,
        },
        {
          login: "pedrorijo91",
          contributions: 1,
        },
        {
          login: "mon",
          contributions: 1,
        },
        {
          login: "walfie",
          contributions: 1,
        },
      ],
    },
    {
      name: "RustScan",
      stars: 5558,
      stargazers_count: 5558,

      owner: {
        login: "RustScan",
      },
      repo: "RustScan",
      contributors: [
        {
          login: "bee-san",
          contributions: 443,
        },
        {
          login: "allcontributors[bot]",
          contributions: 61,
        },
        {
          login: "bernardoamc",
          contributions: 30,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 25,
        },
        {
          login: "CMNatic",
          contributions: 16,
        },
        {
          login: "bergabman",
          contributions: 11,
        },
        {
          login: "niklasmohrin",
          contributions: 10,
        },
        {
          login: "Atul9",
          contributions: 5,
        },
        {
          login: "SuperSandro2000",
          contributions: 5,
        },
        {
          login: "Hydragyrum",
          contributions: 5,
        },
        {
          login: "dmitris",
          contributions: 2,
        },
        {
          login: "SanjoAndPanjo",
          contributions: 2,
        },
        {
          login: "spenserblack",
          contributions: 2,
        },
        {
          login: "eiffel-fl",
          contributions: 2,
        },
        {
          login: "Yogendra0Sharma",
          contributions: 2,
        },
        {
          login: "TGotwig",
          contributions: 2,
        },
        {
          login: "antoinet",
          contributions: 1,
        },
        {
          login: "tim77",
          contributions: 1,
        },
        {
          login: "brumle80",
          contributions: 1,
        },
        {
          login: "freddyb",
          contributions: 1,
        },
        {
          login: "usrGabriel",
          contributions: 1,
        },
        {
          login: "holmes-py",
          contributions: 1,
        },
        {
          login: "Isona",
          contributions: 1,
        },
        {
          login: "luukverhoeven",
          contributions: 1,
        },
        {
          login: "Martin005",
          contributions: 1,
        },
      ],
    },
    {
      name: "sled",
      stars: 5550,
      stargazers_count: 5550,

      owner: {
        login: "spacejam",
      },
      repo: "sled",
      contributors: [
        {
          login: "spacejam",
          contributions: 3329,
        },
        {
          login: "divergentdave",
          contributions: 72,
        },
        {
          login: "dependabot-preview[bot]",
          contributions: 38,
        },
        {
          login: "Kerollmops",
          contributions: 34,
        },
        {
          login: "sitano",
          contributions: 27,
        },
        {
          login: "jeehoonkang",
          contributions: 16,
        },
        {
          login: "roignpar",
          contributions: 11,
        },
        {
          login: "siler",
          contributions: 10,
        },
        {
          login: "mich2000",
          contributions: 10,
        },
        {
          login: "bltavares",
          contributions: 9,
        },
        {
          login: "spacekookie",
          contributions: 9,
        },
        {
          login: "pmuens",
          contributions: 8,
        },
        {
          login: "dependabot[bot]",
          contributions: 8,
        },
        {
          login: "pantonov",
          contributions: 6,
        },
        {
          login: "mbr",
          contributions: 5,
        },
        {
          login: "rubdos",
          contributions: 5,
        },
        {
          login: "nrc",
          contributions: 4,
        },
        {
          login: "Th3Whit3Wolf",
          contributions: 4,
        },
        {
          login: "rleungx",
          contributions: 4,
        },
        {
          login: "lambda",
          contributions: 3,
        },
        {
          login: "jneem",
          contributions: 3,
        },
        {
          login: "lucab",
          contributions: 3,
        },
        {
          login: "asonix",
          contributions: 3,
        },
        {
          login: "Jibbow",
          contributions: 2,
        },
        {
          login: "hdevalence",
          contributions: 2,
        },
      ],
    },
    {
      name: "leaf",
      stars: 5476,
      stargazers_count: 5476,

      owner: {
        login: "autumnai",
      },
      repo: "leaf",
      contributors: [
        {
          login: "hobofan",
          contributions: 63,
        },
        {
          login: "homu",
          contributions: 44,
        },
        {
          login: "MichaelHirn",
          contributions: 7,
        },
        {
          login: "alexandermorozov",
          contributions: 5,
        },
        {
          login: "abacon",
          contributions: 1,
        },
        {
          login: "radarhere",
          contributions: 1,
        },
        {
          login: "palerdot",
          contributions: 1,
        },
        {
          login: "KodrAus",
          contributions: 1,
        },
        {
          login: "dirvine",
          contributions: 1,
        },
        {
          login: "kschmit90",
          contributions: 1,
        },
        {
          login: "killercup",
          contributions: 1,
        },
        {
          login: "wehlutyk",
          contributions: 1,
        },
        {
          login: "vm",
          contributions: 1,
        },
      ],
    },
    {
      name: "ffsend",
      stars: 5474,
      stargazers_count: 5474,

      owner: {
        login: "timvisee",
      },
      repo: "ffsend",
      contributors: [
        {
          login: "timvisee",
          contributions: 837,
        },
        {
          login: "a1346054",
          contributions: 7,
        },
        {
          login: "ignatenkobrain",
          contributions: 6,
        },
        {
          login: "Geweldig",
          contributions: 3,
        },
        {
          login: "murlakatamenka",
          contributions: 3,
        },
        {
          login: "0xflotus",
          contributions: 2,
        },
        {
          login: "vlcinsky",
          contributions: 2,
        },
        {
          login: "0mp",
          contributions: 2,
        },
        {
          login: "axionl",
          contributions: 2,
        },
        {
          login: "robotmachine",
          contributions: 1,
        },
        {
          login: "MagicFab",
          contributions: 1,
        },
        {
          login: "nagyf",
          contributions: 1,
        },
        {
          login: "herbygillot",
          contributions: 1,
        },
        {
          login: "cuviper",
          contributions: 1,
        },
        {
          login: "kslr",
          contributions: 1,
        },
        {
          login: "lilyball",
          contributions: 1,
        },
        {
          login: "nathfreder",
          contributions: 1,
        },
        {
          login: "voider1",
          contributions: 1,
        },
        {
          login: "wilmardo",
          contributions: 1,
        },
        {
          login: "ogarcia",
          contributions: 1,
        },
      ],
    },
    {
      name: "htmlq",
      stars: 5434,
      stargazers_count: 5434,

      owner: {
        login: "mgdm",
      },
      repo: "htmlq",
      contributors: [
        {
          login: "mgdm",
          contributions: 31,
        },
        {
          login: "simonsan",
          contributions: 2,
        },
        {
          login: "chrisdickinson",
          contributions: 1,
        },
        {
          login: "carnott-snap",
          contributions: 1,
        },
        {
          login: "Funami580",
          contributions: 1,
        },
        {
          login: "Alhadis",
          contributions: 1,
        },
        {
          login: "0mp",
          contributions: 1,
        },
        {
          login: "stuartlangridge",
          contributions: 1,
        },
        {
          login: "BasixKOR",
          contributions: 1,
        },
        {
          login: "varunsh-coder",
          contributions: 1,
        },
        {
          login: "heyitsols",
          contributions: 1,
        },
      ],
    },
    {
      name: "pyo3",
      stars: 5293,
      stargazers_count: 5293,

      owner: {
        login: "PyO3",
      },
      repo: "pyo3",
      contributors: [
        {
          login: "davidhewitt",
          contributions: 983,
        },
        {
          login: "kngwyu",
          contributions: 799,
        },
        {
          login: "konstin",
          contributions: 410,
        },
        {
          login: "fafhrd91",
          contributions: 379,
        },
        {
          login: "dgrunwald",
          contributions: 249,
        },
        {
          login: "messense",
          contributions: 142,
        },
        {
          login: "mejrs",
          contributions: 134,
        },
        {
          login: "Alexander-N",
          contributions: 103,
        },
        {
          login: "birkenfeld",
          contributions: 94,
        },
        {
          login: "pganssle",
          contributions: 60,
        },
        {
          login: "nw0",
          contributions: 56,
        },
        {
          login: "alex",
          contributions: 49,
        },
        {
          login: "sebpuetz",
          contributions: 30,
        },
        {
          login: "ijl",
          contributions: 29,
        },
        {
          login: "indygreg",
          contributions: 21,
        },
        {
          login: "batconjurer",
          contributions: 20,
        },
        {
          login: "deantvv",
          contributions: 17,
        },
        {
          login: "macisamuele",
          contributions: 17,
        },
        {
          login: "althonos",
          contributions: 17,
        },
        {
          login: "scalexm",
          contributions: 16,
        },
        {
          login: "ravenexp",
          contributions: 13,
        },
        {
          login: "Vlad-Shcherbina",
          contributions: 12,
        },
        {
          login: "programmerjake",
          contributions: 11,
        },
        {
          login: "m-ou-se",
          contributions: 11,
        },
        {
          login: "Progdrasil",
          contributions: 11,
        },
      ],
    },
    {
      name: "sqlx",
      stars: 5279,
      stargazers_count: 5279,

      owner: {
        login: "launchbadge",
      },
      repo: "sqlx",
      contributors: [
        {
          login: "mehcode",
          contributions: 963,
        },
        {
          login: "abonander",
          contributions: 206,
        },
        {
          login: "danielakhterov",
          contributions: 114,
        },
        {
          login: "jplatte",
          contributions: 88,
        },
        {
          login: "pimeys",
          contributions: 46,
        },
        {
          login: "Freax13",
          contributions: 33,
        },
        {
          login: "JesperAxelsson",
          contributions: 32,
        },
        {
          login: "matt-paul",
          contributions: 17,
        },
        {
          login: "izik1",
          contributions: 16,
        },
        {
          login: "raftario",
          contributions: 13,
        },
        {
          login: "esemeniuc",
          contributions: 11,
        },
        {
          login: "thedodd",
          contributions: 10,
        },
        {
          login: "Ace4896",
          contributions: 10,
        },
        {
          login: "joshtriplett",
          contributions: 9,
        },
        {
          login: "blackwolf12333",
          contributions: 9,
        },
        {
          login: "utter-step",
          contributions: 9,
        },
        {
          login: "phated",
          contributions: 8,
        },
        {
          login: "paolobarbolini",
          contributions: 8,
        },
        {
          login: "salewski",
          contributions: 7,
        },
        {
          login: "andrewwhitehead",
          contributions: 7,
        },
        {
          login: "framp",
          contributions: 7,
        },
        {
          login: "meteficha",
          contributions: 7,
        },
        {
          login: "markazmierczak",
          contributions: 7,
        },
        {
          login: "PoiScript",
          contributions: 7,
        },
        {
          login: "xiaopengli89",
          contributions: 7,
        },
      ],
    },
  ],
};
