const BusFactoryFinder = require("../../src/domain/BusFactorFinder");
const assert = require("assert");

describe("BusFactorFinder", () => {
  it("should throw error if min threshold is not defined", async () => {
    assert.throws(() => BusFactoryFinder({}));
  });

  it("should calculate bus factor", async () => {
    const projects = [
      {
        name: "ripgrep",
        contributors: [
          {
            login: "BurntSushi",
            contributions: 78,
          },
          {
            login: "okdana",
            contributions: 15,
          },
          {
            login: "balajisivaraman",
            contributions: 5,
          },
          {
            login: "tiehuis",
            contributions: 1,
          },
          {
            login: "ericbn",
            contributions: 1,
          },
        ],
      },
    ];
    const busFactorFinder = BusFactoryFinder({ min_threshold: 0.75 });
    const results = busFactorFinder.find(projects);
    assert.strictEqual(results.length, 1);
    assert.strictEqual(results[0].project, "ripgrep");
    assert.strictEqual(results[0].user, "BurntSushi");
    assert.strictEqual(results[0].percentage, 0.78);
  });

  it("should not return project if percentage is not greater than min threshold", async () => {
    const projects = [
      {
        name: "ripgrep",
        contributors: [
          {
            login: "BurntSushi",
            contributions: 78,
          },
          {
            login: "okdana",
            contributions: 15,
          },
          {
            login: "balajisivaraman",
            contributions: 5,
          },
          {
            login: "tiehuis",
            contributions: 1,
          },
          {
            login: "ericbn",
            contributions: 1,
          },
        ],
      },
    ];
    const busFactorFinder = BusFactoryFinder({ min_threshold: 0.79 });
    const results = busFactorFinder.find(projects);
    assert.strictEqual(results.length, 0);
  });

  it("should calculate bus factor for disordered contributors", async () => {
    const projects = [
      {
        name: "ripgrep",
        contributors: [
          {
            login: "ericbn",
            contributions: 1,
          },
          {
            login: "okdana",
            contributions: 15,
          },
          {
            login: "balajisivaraman",
            contributions: 5,
          },
          {
            login: "tiehuis",
            contributions: 1,
          },
          {
            login: "BurntSushi",
            contributions: 78,
          },
        ],
      },
    ];
    const busFactorFinder = BusFactoryFinder({ min_threshold: 0.75 });
    const results = busFactorFinder.find(projects);
    assert.strictEqual(results.length, 1);
    assert.strictEqual(results[0].project, "ripgrep");
    assert.strictEqual(results[0].user, "BurntSushi");
    assert.strictEqual(results[0].percentage, 0.78);
  });

  it("should return empty array for empty projects", async () => {
    const busFactorFinder = BusFactoryFinder({ min_threshold: 0.75 });
    const results = busFactorFinder.find([]);
    assert.strictEqual(results.length, 0);
  });
});
