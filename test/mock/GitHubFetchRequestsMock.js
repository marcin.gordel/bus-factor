const ProjectsFixtures = require("../fixtures/Projects");

const ResponseMock = (data, status = 200) => ({
  status,
  headers: new Map([["content-type", "application/json"]]),
  json: () => data,
});

/*
 * Simple primitive mock for test purpose not included pagination and project_count limit
 * only returned rust project fixtures
 */
module.exports = async (url_str) => {
  const url = new URL(url_str);
  if (url.pathname === "/rate_limit") {
    return ResponseMock({
      resources: {
        core: {
          remaining: 5000,
          reset: new Date().valueOf() + 60000,
        },
        search: {
          remaining: 7,
          reset: new Date().valueOf() + 60000,
        },
      },
    });
  } else if (url.pathname === "/search/repositories") {
    const language = url.searchParams.get("q")?.split(":")?.[1];
    const page = url.searchParams.get("page");
    if (page !== "1") {
      return ResponseMock({ items: [] });
    }
    if (language !== "rust") {
      throw new Error(
        'Validation Failed: {"message":"None of the search qualifiers apply to this search type.","resource":"Search","field":"q","code":"invalid"}'
      );
    }
    return ResponseMock(ProjectsFixtures);
  } else if (url.pathname.indexOf("/repos") === 0) {
    const owner = url[Object.getOwnPropertySymbols(url)[0]].path[1];
    const repo = url[Object.getOwnPropertySymbols(url)[0]].path[2];
    const contributors = ProjectsFixtures.items?.find((proj) => proj.repo === repo && proj.owner.login === owner)?.contributors;
    return ResponseMock(contributors);
  } else {
    throw new Error("Github Mock Error");
  }
};
