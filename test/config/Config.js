const GitHubFetchRequestsMock = require("../mock/GitHubFetchRequestsMock");

module.exports = {
  min_threshold: 0.75,
  providers: {
    github: {
      auth: "test_mock",
      request: {
        fetch: GitHubFetchRequestsMock,
      },
      sort_by: "stars",
    },
  },
};
