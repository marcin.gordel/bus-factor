const BusFactorService = require("../../src/services/BusFactorService");
const BusFactoryFinder = require("../../src/domain/BusFactorFinder");
const GitHubProvider = require("../../src/providers/GitHubProvider");
/*
 * We can defined separate configurations for tests e.g. I defined logger mock
 */
module.exports = (config) => {
  const githubProvider = GitHubProvider(config);
  const busFactoryFinder = BusFactoryFinder(config);
  const logger = () => null;
  const busFactoryService = BusFactorService(githubProvider, busFactoryFinder, logger);
  return {
    busFactoryService,
    logger,
  };
};
