const config = require("../config/Config");
const DI = require("../config/DI")(config);
const assert = require("assert");

describe("BusFactorService", () => {
  const busFactorService = DI.busFactoryService;

  it("should find projects with bus factor", async () => {
    const results = await busFactorService.search("rust", 50);
    assert.strictEqual(results.length, 31);
    assert.strictEqual(results[0].project, "ripgrep");
    assert.strictEqual(results[0].user, "BurntSushi");
    assert.strictEqual(results[0].percentage, 0.89);
    assert.strictEqual(results[1].project, "swc");
    assert.strictEqual(results[1].user, "kdy1");
    assert.strictEqual(results[1].percentage, 0.77);
  });

  it("should not find projects for invalid language", async () => {
    const error = await busFactorService.search("invalid_lang", 40).catch((error) => error);
    assert.strictEqual(
      error.toString(),
      'HttpError: Validation Failed: {"message":"None of the search qualifiers apply to this search type.","resource":"Search","field":"q","code":"invalid"}'
    );
  });

  it("should return error if project_count is greater than 1000 (max github limit)", async () => {
    const error = await busFactorService.search("rust", 1001).catch((error) => error);
    assert.strictEqual(error.toString(), "Error: GitHub Search API provides up to 1,000 results for each search. Try to use a smaller one.");
  });

  it("should return error if project_count is greater than current remaining rate limit", async () => {
    const error = await busFactorService.search("rust", 7 * 100 + 1).catch((error) => error);
    assert.strictEqual(
      error.toString(),
      "Error: Project count is greater than your remaining GitHub Search Api rate limit. Try to use a smaller one."
    );
  });

  it("should return error if github auth token is not defined", async () => {
    const invalidConfig = JSON.parse(JSON.stringify(config));
    delete invalidConfig.providers.github.auth;
    let expectedError;
    try {
      require("../../src/config/DI")(invalidConfig);
    } catch (error) {
      expectedError = error;
    } finally {
      assert.strictEqual(expectedError?.toString(), "Error: Github token is not defined in the configuration");
    }
  });
});
